<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Application</name>
    <message>
        <source>Collections</source>
        <translation>Коллекции</translation>
    </message>
    <message>
        <source>Add...</source>
        <translation>Добавить...</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <source>Stop server</source>
        <translation>Выключить сервер</translation>
    </message>
    <message>
        <source>Restart server</source>
        <translation>Включить сервер</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Завершить</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Search pattern...</source>
        <translation>Параметры поиска...</translation>
    </message>
    <message>
        <source>hashprint:</source>
        <translation>Отпечаток:</translation>
    </message>
    <message>
        <source>save time:</source>
        <translation>Сохранено:</translation>
    </message>
    <message>
        <source>source:</source>
        <translation>Источник:</translation>
    </message>
    <message>
        <source>Locate</source>
        <translation>Обнаружить</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>Collection</name>
    <message>
        <source>Pending...</source>
        <translation>Ожидание...</translation>
    </message>
    <message>
        <source>Checking hash...</source>
        <translation>Сравнение хэша...</translation>
    </message>
    <message>
        <source>Same hash was found</source>
        <translation>Такой же хэш был найден</translation>
    </message>
    <message>
        <source>Processing tag</source>
        <translation>Добавление тэга</translation>
    </message>
    <message>
        <source>Adding media...</source>
        <translation>Добавление медиа...</translation>
    </message>
    <message>
        <source>Attaching tag</source>
        <translation>Назначение тэга</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>SaveRequest</name>
    <message>
        <source>Save picture</source>
        <translation>Сохранить изображение</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>page:</source>
        <translation>Страница:</translation>
    </message>
    <message>
        <source>Could not save temp data into %1</source>
        <translation>Не удается открыть файл для записи %1</translation>
    </message>
    <message>
        <source>Save from %1</source>
        <translation>Сохранить с %1</translation>
    </message>
    <message>
        <source>origin:</source>
        <translation>Источник:</translation>
    </message>
    <message>
        <source>Unknown resource type %1</source>
        <translation>Недопустимый тип ресурса: %1</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <source>script shells</source>
        <translation>Оболочки</translation>
    </message>
    <message>
        <source>Add shell</source>
        <translation>Добавить оболочку</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Темная</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <source>Revert</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <source>Port to listen signals on</source>
        <translation>Порт для прослушивания сигналлов из браузеров</translation>
    </message>
    <message>
        <source>Add new shell for script execution</source>
        <translation>Добавить новую оболочку для исполнения скриптов</translation>
    </message>
    <message>
        <source>Window color scheme</source>
        <translation>Цветовая палитра окон</translation>
    </message>
    <message>
        <source>Reset all settings to default state</source>
        <translation>Восстановить все настройки в изначальное состояние</translation>
    </message>
    <message>
        <source>Reset all settings to latest saved state</source>
        <translation>Восстановить все настройки в последнее сохраненное состояние</translation>
    </message>
    <message>
        <source>Save  and apply all settings</source>
        <translation>Сохранить и применить настройки</translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Collection item from %1</source>
        <translation>Элемент коллекции %1</translation>
    </message>
</context>
</TS>
