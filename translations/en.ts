<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Application</name>
    <message>
        <source>Collections</source>
        <translation>Collections</translation>
    </message>
    <message>
        <source>Add...</source>
        <translation>Add...</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Settings...</translation>
    </message>
    <message>
        <source>Stop server</source>
        <translation>Stop server</translation>
    </message>
    <message>
        <source>Restart server</source>
        <translation>Restart server</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Search pattern...</source>
        <translation>Search pattern...</translation>
    </message>
    <message>
        <source>hashprint:</source>
        <translation>Hashprint:</translation>
    </message>
    <message>
        <source>save time:</source>
        <translation>Save time:</translation>
    </message>
    <message>
        <source>source:</source>
        <translation>Source:</translation>
    </message>
    <message>
        <source>Locate</source>
        <translation>Locate</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>Collection</name>
    <message>
        <source>Pending...</source>
        <translation>Pending...</translation>
    </message>
    <message>
        <source>Same hash was found</source>
        <translation>Same hash was found</translation>
    </message>
    <message>
        <source>Processing tag</source>
        <translation>Adding tag</translation>
    </message>
    <message>
        <source>Adding media...</source>
        <translation>Adding media...</translation>
    </message>
    <message>
        <source>Attaching tag</source>
        <translation>Attaching tag</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Done</translation>
    </message>
    <message>
        <source>Checking hash...</source>
        <translation>Checking hash...</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
<context>
    <name>SaveRequest</name>
    <message>
        <source>Save picture</source>
        <translation>Save picture</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Unknown error</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>page:</source>
        <translation>Page:</translation>
    </message>
    <message>
        <source>Could not save temp data into %1</source>
        <translation>Could not save temp data into %1</translation>
    </message>
    <message>
        <source>origin:</source>
        <translation>Origin:</translation>
    </message>
    <message>
        <source>Save from %1</source>
        <translation>Save from %1</translation>
    </message>
    <message>
        <source>Unknown resource type %1</source>
        <translation>Unknown resource type: %1</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <source>port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>script shells</source>
        <translation>Script shells</translation>
    </message>
    <message>
        <source>Add shell</source>
        <translation></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation></translation>
    </message>
    <message>
        <source>Dark</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <source>Revert</source>
        <translation></translation>
    </message>
    <message>
        <source>Apply</source>
        <translation></translation>
    </message>
    <message>
        <source>Port to listen signals on</source>
        <translation>Port that should be used to listen signals from browsers</translation>
    </message>
    <message>
        <source>Add new shell for script execution</source>
        <translation></translation>
    </message>
    <message>
        <source>Window color scheme</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset all settings to default state</source>
        <translation></translation>
    </message>
    <message>
        <source>Reset all settings to latest saved state</source>
        <translation></translation>
    </message>
    <message>
        <source>Save  and apply all settings</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Collection item from %1</source>
        <translation>Collection item from %1</translation>
    </message>
</context>
</TS>
