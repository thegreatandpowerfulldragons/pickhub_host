QT += core gui widgets network sql multimediawidgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

VPATH += src \
         headers \
         ui

INCLUDEPATH += headers

SOURCES += \
    application.cpp \
    browser.cpp \
    clickablelabel.cpp \
    collection.cpp \
    dss.cpp \
    previewvideo.cpp \
    spiner.cpp \
    timeline.cpp \
    itemview.cpp \
    view.cpp \
    previewanimation.cpp \
    previewimage.cpp \
    previewbase.cpp \
    shellrow.cpp \
    hashprint.cpp \
    http.cpp \
    main.cpp \
    rowcontrol.cpp \
    saverequest.cpp \
    server.cpp \
    settings.cpp \
    settingswindow.cpp \
    sql.cpp \

HEADERS += \
    application.h \
    browser.h \
    clickablelabel.h \
    collection.h \
    dss.h \
    previewvideo.h \
    spiner.h \
    timeline.h \
    itemview.h \
    view.h \
    previewanimation.h \
    previewimage.h \
    previewbase.h \
    shellrow.h \
    hashprint.h \
    http.h \
    rowcontrol.h \
    saverequest.h \
    server.h \
    settings.h \
    settingswindow.h \
    sql.h

FORMS += \
    browser.ui \
    clickablelabel.ui \
    previewvideo.ui \
    spiner.ui \
    view.ui \
    previewanimation.ui \
    previewimage.ui \
    saverequest.ui \
    settingswindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

TRANSLATIONS += \
    translations/en.ts\
    translations/ru.ts
