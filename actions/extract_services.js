const fs = require('./fs.js')
const path = require('path')

const from = process.argv[2]
const to = process.argv[3]

fs.remove(to)
fs.createDir(to)

const services = fs.list(from)
for (let index in services) {
  source = from + '/' + services[index]
  target = to + '/' + services[index]

  process.stdout.write(`Copying service ${path.basename(source)}... `)

  fs.copy(source, target)

  process.stdout.write(`done!\n`)
}
