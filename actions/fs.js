const fs = require('fs')

function exists(target) {
  return fs.existsSync(target)
}

function list(dir) {
  return fs.readdirSync(dir)
}

function type(target) {
  const stat = fs.statSync(target)
  if (stat.isDirectory()) return 'dir'
  if (stat.isFile()) return 'file'
  if (stat.isBlockDevice()) return 'block'
  if (stat.isCharacterDevice()) return 'char'
  if (stat.isFIFO()) return 'fifo'
  if (stat.isSocket()) return 'socket'
  if (stat.isSymbolicLink()) return 'link'
}

function createDir (dir) {
  if (!exists(dir)) {
    const parts = dir.split(/[\\\/]/)
    let path = ''
    for (const i in parts) {
      path += parts[i] + '/'
      if (!exists(path)) {
        fs.mkdirSync(path)
      }
    }
  }
}

function remove(target) {
  if(exists(target)) {
    if (type(target) == 'dir') {
      const files = list(target)

      for (let i in files) {
        remove(target + '/' + files[i])
      }

      fs.rmdirSync(target)
    } else {
      fs.unlinkSync(target)
    }
  }
}

function copy(from, to) {
  if (exists(to)) {
    remove(to)
  }

  switch (type(from)) {
    case 'dir':
      const files = list(from)

      createDir(to)

      for (let i in files) {
        const source = from + '/' + files[i]
        const target = to + '/' + files[i]

        copy(source, target)
      }
      break;
    case 'file':
      fs.copyFileSync(from, to)
      break;
  }
}

module.exports.exists    = exists
module.exports.list      = list
module.exports.type      = type
module.exports.remove    = remove
module.exports.copy      = copy
module.exports.createDir = createDir
