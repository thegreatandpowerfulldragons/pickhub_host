const fs = require('./fs.js')
const path = require('path')

const from = process.argv[2]
const to = process.argv[3]

fs.remove(to)
fs.createDir(to)

const files = fs.list(from)
for (let index in files) {
  let file = files[index]
  if (!file.endsWith('.qm')) continue

  const basename = path.parse(file).name

  source = from + '/' + file
  target = to + '/' + basename

  process.stdout.write(`Copying locale ${basename}... `)

  fs.copy(source, target)

  process.stdout.write(`done!\n`)
}
