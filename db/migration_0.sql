CREATE TABLE tags
(
  id      INTEGER NOT NULL,
  name    TEXT    NOT NULL UNIQUE,
  PRIMARY KEY(id AUTOINCREMENT)
);

CREATE TABLE media
(
  id       INTEGER NOT NULL,
  format   TEXT    NOT NULL,
  hash     BLOB            ,
  source   TEXT    NOT NULL,
  page     TEXT    NOT NULL,
  time     INTEGER NOT NULL,
  type     TEXT    NOT NULL,
  PRIMARY  KEY(id AUTOINCREMENT)
);

CREATE TABLE media_tag
(
  media INTEGER NOT NULL,
  tag   INTEGER NOT NULL,
  PRIMARY KEY(media, tag)
);

PRAGMA user_version = 1;
