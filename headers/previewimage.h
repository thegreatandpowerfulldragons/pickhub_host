#ifndef PREVIEWIMAGE_H
#define PREVIEWIMAGE_H

#include "previewbase.h"

namespace Ui {
class PreviewImage;
}

class PreviewImage : public PreviewBase
{
    Q_OBJECT
public:
    explicit PreviewImage(QWidget* parent = nullptr);
    ~PreviewImage();

    virtual void open(const QString &filepath) override;

protected:
    virtual void resizeEvent(QResizeEvent *event) override;

private:
    void redraw();

    Ui::PreviewImage *ui;

    QPixmap pixmap;
};

#endif // PREVIEWIMAGE_H
