#ifndef QSS_H
#define QSS_H

#include <QtCore>
#include <QFile>

class DSS : public QObject
{
    Q_OBJECT
public:
    explicit DSS(QObject* parent = nullptr);
    DSS(const QString& path, QObject* parent = nullptr);


    void load(const QString& dss);
    void loadFromFile(const QString& path);

    void bindParam(const QString& name, const QString& value);
    void bindParams(const QMap<QString, QString>& paramsMap);

    QString getParam(const QString& name, const QString& defaultValue = "");

    const QString& toString() const;

    operator QString&();

signals:
    void updated();

private:
    void process();

    QString source;
    QString cache;
    QMap<QString, QString> params;

    const QRegularExpression paramRX = QRegularExpression("@(?<name>\\w+)");
    const QRegularExpression eachRX = QRegularExpression("each\\s*\\((?<inputs>[^()]*)\\)(?<modifier>:\\w*)?");
};

#endif // QSS_H
