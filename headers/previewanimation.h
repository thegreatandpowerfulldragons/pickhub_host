#ifndef PREVIEWANIMATION_H
#define PREVIEWANIMATION_H

#include "previewbase.h"
#include <QGraphicsView>
#include <QMovie>

namespace Ui {
class PreviewAnimation;
}

class PreviewAnimation : public PreviewBase
{
    Q_OBJECT
public:
    explicit PreviewAnimation(QWidget* parent = nullptr);
    ~PreviewAnimation();

    virtual void open(const QString &filepath) override;

public slots:
    void pauseToggle();
    void setFrame(int time);

protected:
    virtual void resizeEvent(QResizeEvent* event) override;

private:
    void updateImage();
    void updateSize();

    Ui::PreviewAnimation *ui;
    QMovie* movie;
    QSize targetSize;

    bool stateBeforeTimeDrag;

private slots:
    void playerStateChanged(QMovie::MovieState state);
    void playerFrameChanged(int frameNumber);
    void playerOriginResized(const QSize &size);
    void playerError(QImageReader::ImageReaderError error);

    void timeDragStart();
    void timeDrag(int time);
    void timeDragEnd();
};

#endif // PREVIEWANIMATION_H
