#ifndef COLLECTION_H
#define COLLECTION_H

#include <QObject>
#include <QSqlDatabase>
#include <QFuture>
#include <QDateTime>
#include <QFileSystemWatcher>
#include <QAbstractVideoSurface>
#include <QPixmap>

class HashPrint;

class Collection : public QObject
{
    class VideoCapture : public QAbstractVideoSurface
    {
    public:
        virtual QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const override;

        virtual bool present(const QVideoFrame& frame) override;

        const QPixmap& getPixmap();

    private:
        QPixmap pixmap;
    };

    static QMap<QString, Collection*> collections;

    Q_OBJECT
public:
    enum class ItemType
    {
        Image     = 2,
        Animation = 4,
        Video     = 8,
        Audio     = 16
    };
    Q_ENUM(ItemType)

    struct Item
    {
        Item() = default;

        uint id          = {};
        QString format   = {};
        HashPrint* hash  = {};
        QString source   = {};
        QString page     = {};
        QDateTime time   = {};
        ItemType type    = {};
        QStringList tags = {};
    };

    struct ItemShort
    {
        ItemShort() = default;
        ItemShort(const Item& item)
            : id(item.id)
            , type(item.type)
        {}

        uint id       = {};
        ItemType type = {};
    };

    class Paths
    {
    public:
        Paths(const QString& path);

        QString getPath(const QString& subpath = "") const;
        QString getMetaPath() const;
        QString getDatabasePath() const;
        QString getDatabaseBackupPath() const;
        QString getCommitLockPath() const;

    private:
        QString path;
    };

    enum class CommitState
    {
        Pending    = 2,
        Processing = 4,
        Finished   = 8,
        Errored    = 16,
        Canceled   = 32
    };

    const uint sqlVersion = 1;

    static Collection* get(const QString& path);
    static void close(const QString& path);

    // Use this object only for repository verification
    Collection(const QString& path);
    ~Collection();

    void watch();
    void unwatch();

    bool isValid();

    bool isVerified() const;

    QString getName() const;
    uint getVersion();

    int commit(const QString& tempFilePath, const QUrl& source, const QUrl& page, const QStringList& tags, ItemType type);
    void cancelCommit(int commitKey);
    bool remove(uint id);

    bool getItem(uint id, Item& result);

    bool searchItems(QVector<ItemShort>& out, const QString& searchPattern = "");
    bool getAllItems(QVector<ItemShort>& out);

    bool fix();

    static QPixmap generateThumbnail(const QString& filepath, ItemType type);

    const Paths paths;

signals:
    void commitFinished(int commitKey, bool state);
    void commitProgress(int commitKey, uint value, uint total, QString comment);

    void itemAdded(Collection::Item item);
    void itemRemoved(uint id);

    void deletedByUser();

    void thumbnailGenerated(const QPixmap& thumbnail);

private:
    const char* databaseInaccessibleString() const;

    void watch(const QString& file);
    void unwatch(const QString& file);

    void backup();
    void restore();

    bool verified;

    QFileSystemWatcher* watcher;
    QStringList watchPaths;

    QSqlDatabase database;

    QVector<CommitState> commitSchedule;
};

#endif // COLLECTION_H
