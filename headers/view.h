#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include "collection.h"

class PreviewBase;
class Application;

namespace Ui {
class View;
}

class View : public QWidget
{
    Q_OBJECT
public:
    explicit View(Collection* collection, uint id);
    ~View();

protected:
    virtual void showEvent(QShowEvent* event) override;
    virtual void closeEvent(QCloseEvent* event) override;

private:
    QString getFileName() const;

    Ui::View *ui;
    PreviewBase* preview;

    Collection* collection;

    uint id;
};

#endif // VIEW_H
