#ifndef ITEMVIEW_H
#define ITEMVIEW_H

#include <QWidget>
#include <QMap>
#include <QSpacerItem>
#include <QScrollBar>
#include "collection.h"

class ItemView : public QWidget
{
    Q_OBJECT
public:
    enum class DisplayMode
    {
        All,
        Specific
    };

    struct Item
    {
        QPixmap pixmap;
        Collection::ItemType type;
    };

    struct Selection
    {
        int row;
        int col;

        Selection left()
        {
            return {row, col - 1};
        }

        Selection right()
        {
            return  {row, col + 1};
        }

        Selection up()
        {
            return {row - 1, col};
        }

        Selection down()
        {
            return {row + 1, col};
        }
    };

    explicit ItemView(QWidget* parent = nullptr);
    ~ItemView();

    void setupThumbnails(const QMap<int, Item>& thumbnails);
    void addThumbnail(int id, const Item& thumbnail, bool display);
    void updateThumbnail(int id, const QPixmap& thumbnail);
    void removeThumbnail(int id);

    int getItemSize() const;
    void setItemSize(int value);

    int getItemSpace() const;
    void setItemSpace(int value);

    void DisplaySpecific(const QVector<int>& indexes);
    void displayAll();
    DisplayMode getDisplayMode() const;

    static void drawItem(QPainter& painter, const Item& item, const QRect& rect, const QColor& firstColor, const QColor& secondColor);

protected:
    virtual void paintEvent(QPaintEvent* event) override;
    virtual void resizeEvent(QResizeEvent* event) override;
    virtual void wheelEvent(QWheelEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent* event) override;
    virtual void contextMenuEvent(QContextMenuEvent* event) override;

signals:
    void itemSelected(int id);

    void itemOpen(int id);
    void itemLocate(int id);
    void itemDelete(int id);

    void openSource(int id);
    void searchWithSameHashprint(int id);

    void addToAnotherCollection(int id);
    void moveToAnotherCollection(int id);

private:
    QRect getDisplayRect() const;
    int getBoxSize() const;

    Selection getItemUnderPoint(const QPoint& point);
    void select(const Selection& selection);

    void showContext(const QPoint& position);

    void updateModel();
    void addItemToModel();
    void recalculateRowDimensions();

    QWidget* spacer;
    QScrollBar* scrollbar;

    int itemSize;
    int itemSpace;

    QMap<int, Item> thumbnails;
    QVector<QVector<int>> model;
    Selection selection;
    int selectedId;

    DisplayMode displayMode;
    QVector<int> displayItems;

    QSizeF cachedCellSize;
    int cachedColumnCount;
};

#endif // ITEMVIEW_H
