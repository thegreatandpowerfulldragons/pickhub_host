#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QIcon>
#include <QSystemTrayIcon>
#include <QAction>
#include <QLockFile>

class Settings;
class Server;
class SettingsWindow;
class Collection;
class DSS;
class Browser;

class Application : public QApplication
{
    Q_OBJECT
public:
    enum class StartupFailState
    {
        None,
        AlreadyStarted,
        UnableToLoadLocale
    };

    Application(int& argc, char**& argv);
    ~Application();

    static const QIcon& getIcon();
    static const QIcon& getIconOnline();
    static const QIcon& getIconOffline();

    static const QFont& getFontAwesomeFont();

    static void setupTrayIconOnline();
    static void setupTrayIconOffline();

    static Settings* getSettings();
    static Server* getServer();

    static void setupTrayRestart();
    static void setupTrayStop();

    static QString getLocalPath(const QString& filename);
    static QString getTempDir();
    static QString getCollectionsPath();
    static QString getSettingsPath();

    static const QStringList& getCollectionsPaths();
    static void addCollectionPath(const QString& path);
    static void removeCollectionPath(const QString& path);

    static DSS* getDss();
    static StartupFailState getFailState();

public slots:
    void showSettings();
    void showCollection(const QString& path);
    void showCollectionAdd();
    void updateLanguage();

private:
    void attachCollection(const QString& path);
    void detachCollection(int index);

    QString getOsName() const;

    QLockFile* lock;
    static Application* instance;

    QIcon icon;
    QIcon iconOnline;
    QIcon iconOffline;

    QFont fontAwesomeFont;

    QTranslator* translator;
    QSystemTrayIcon* trayIcon;
    QAction* trayStop;
    QAction* trayRestart;
    QMenu* trayCollections;
    QAction* trayCollectionsAdd;
    QAction* trayCollectionsSeparator;
    Settings* settings;
    Server* server;
    SettingsWindow* settingsWindow;
    DSS* dss;
    QMap<QString, Browser*> browsers;

    QStringList collectionsPaths;

    StartupFailState failState;
};

#endif // APPLICATION_H
