#ifndef BROWSER_H
#define BROWSER_H

#include <QMainWindow>
#include <QStandardItem>
#include "collection.h"

class Application;
class DSS;

namespace Ui {
class Browser;
}

class Browser : public QMainWindow
{
    Q_OBJECT
public:
    explicit Browser(Collection* collection);
    ~Browser();

    QSize getItemSize() const;

public slots:
    void selectItem(uint id);

    void itemAdded(const Collection::Item& item);
    void itemRemoved(uint id);

    void initItems();
    void searchItems();
    void clearSearch();

    void dssUpdated();

protected:
    virtual void closeEvent(QCloseEvent* event) override;
    virtual void keyPressEvent(QKeyEvent* event) override;

private:
    QPixmap getThumbnail(const Collection::ItemShort& item);

    void locateItem(int index);
    void openItem(int index);
    void deleteItem(int index);

    Ui::Browser *ui;

    Collection* collection;

    Collection::Item selectedItem;
};

#endif // BROWSER_H
