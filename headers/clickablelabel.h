#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>

class ClickableLabel : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString icon READ getIcon WRITE setIcon NOTIFY iconChanged)
public:
    explicit ClickableLabel(QWidget* parent = nullptr);
    ClickableLabel(const QString& text, const QPixmap& pixmap = QPixmap(), QWidget* parent = nullptr);
    ~ClickableLabel();

    void setText(const QString& value);
    QString getText() const;

    void setIcon(const QString& unicodeChar);
    QString getIcon() const;

    void setFont(const QFont& value);
    QFont getFont() const;

signals:
    void textChanged(const QString& value);
    void iconChanged(const QString& value);

    void clicked();

protected:
    void enterEvent(QEvent* event);
    void leaveEvent(QEvent* event);
    void mousePressEvent(QMouseEvent*);

private:
    QLabel* textLabel;
    QLabel* iconLabel;

    bool wasOver;
};

#endif // CLICKABLELABEL_H
