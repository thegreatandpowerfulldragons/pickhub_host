#ifndef SAVEREQUEST_H
#define SAVEREQUEST_H

#include <QProcess>
#include <QNetworkReply>
#include <QMainWindow>
#include <QFile>
#include <QLineEdit>
#include "collection.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SaveRequest; }
QT_END_NAMESPACE

class Server;
class Collection;
class PreviewBase;

class SaveRequest : public QMainWindow
{
    Q_OBJECT
public:
    SaveRequest(Server* server);
    ~SaveRequest();

    void beginLoading(const QString& imageUrl, const QString& pageUrl);

    void syncCollections();

public slots:
    void loadingProgress(qint64 value, qint64 total);
    void loadingReadyRead();
    void loadingFinished(QNetworkReply* reply);

    void tagsLoaded(int code);

    void confirm();
    void cancel();

    void dssUpdated();

protected:
    void closeEvent(QCloseEvent* event);

private:
    void createField(const QString& content = "");
    bool hasTag(const QString& tag, QLineEdit* fieldToIgnore = nullptr);

    void setupLoading();
    void setupError(const QString& error);
    void setupLoaded();

    void setProgress(quint64 value, quint64 total);

    void updateStatus();

    QString getCurrentCollectionPath() const;

    static QString latestCollection;

    Ui::SaveRequest* ui;

    QUrl origin;
    QUrl page;
    QString filename;
    int commitKey;
    Collection::ItemType resourceType;

    QProcess* tagLoader;
    QNetworkAccessManager* manager;
    QNetworkReply* reply;

    QLineEdit* leadingTagField;
    PreviewBase* preview;

    Server* server;

    QByteArray loadBytes;

    bool loaded;
    bool closingDelayed;

    QMap<QString, QString> collections;
};

#endif // SAVEREQUEST_H
