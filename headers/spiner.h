#ifndef SPINER_H
#define SPINER_H

#include <QWidget>

namespace Ui {
    class Spiner;
}

class Spiner : public QWidget
{
    Q_OBJECT
public:
    explicit Spiner(QWidget *parent = nullptr);
    ~Spiner();

protected:
    virtual void paintEvent(QPaintEvent* event) override;

private:
    void tick();

    Ui::Spiner* ui;
    uint counter;
};

#endif // SPINER_H
