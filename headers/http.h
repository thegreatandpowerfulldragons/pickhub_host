#ifndef REQUESTBODY_H
#define REQUESTBODY_H

#include <QtCore>
#include <QJsonObject>

class HTTP : public QObject
{
    Q_OBJECT
public:
    struct Header
    {
        Header(const QString& value)
            : value(value)
        {}

        Header(const QString& value, const QMap<QString, QString>& params)
            : value(value)
            , params(params)
        {}

        Header operator =(const QString &rhs)
        {
            return Header(rhs);
        }

        operator QString()
        {
            return value;
        }

        operator QMap<QString, QString>()
        {
            return params;
        }

        QString operator [](const QString& index)
        {
            if (params.contains(index)) return params[index];
            return "";
        }

    private:
        QString value;
        QMap<QString, QString> params;
    };

    void parseFrom(const QString& input);

    const QMap<QString, QString>& getHeaders() const;

    QString getHeader(const QString& name);
    void setHeader(const QString& name, const QString& value);

    void setBody(const QString& value);
    const QString& getBody();

    virtual QString toString();

protected:
    virtual void parseStart(const QString& line) = 0;
    virtual void ParseHeader(const QString& line);
    virtual void ParseBody(const QString& text) = 0;

    virtual QString startToString() = 0;

private:
    QMap<QString, QString> headers;
    QString body;
};

class HTTPRequest : public HTTP
{
    Q_OBJECT
public:
    enum Method
    {
        GET,
        HEAD,
        POST,
        PUT,
        DELETE,
        CONNECT,
        OPTIONS,
        TRACE,
        PATCH,
    };
    Q_ENUM(Method)

    void setMethod(Method value);
    Method getMethod() const;
    QString getMethodName() const;

    void setVersion(double value);
    double getVersion() const;

    void setPath(const QStringList& value);
    const QStringList& getPath() const;

    const QJsonObject& getData();

protected:
    virtual void parseStart(const QString& line) override;
    virtual void ParseBody(const QString &text) override;

    virtual QString startToString() override;

private:
    void tryParseBodyToData();

    Method method;
    double version;
    QStringList path;
    QJsonObject data;
};

class HTTPResponse : public HTTP
{
    Q_OBJECT
public:
    enum Status
    {
        Continue = 100,
        SwitchingProtocols = 101,
        Processing = 102,

        OK = 200,
        Created = 201,
        Accepted = 202,
        NonAuthoritativeInformation = 203,
        NoContent = 204,
        ResetContent = 205,
        PartialContent = 206,
        MultiStatus = 207,
        AlreadyReported = 208,
        IMUsed = 226,

        MultipleChoices = 300,
        MovedPermanently = 301,
        MovedTemporarily = 302,
        SeeOther = 303,
        NotModified = 304,
        UseProxy = 305,
        TemporaryRedirect = 307,
        PermanentRedirect = 308,

        BadRequest = 400,
        Unauthorized = 401,
        PaymentRequired = 402,
        Forbiden = 403,
        NotFound = 404,
        MethodNotAllowed = 405,
        NotAcceptable = 406,
        ProxyAuthenticationRequired = 407,
        RequestTimeout = 408,
        Conflict = 409,
        Gone = 410,
        LengthRequired = 411,
        PreconditionFailed = 412,
        PayloadTooLarge = 413,
        URITooLong = 414,
        UnsupportedMediaType = 415,
        RangeNotSatisfiable = 416,
        ExpectationFailed = 417,
        IAmATeapot = 418,
        AuthenticationTimeout = 419,
        MisdirectedRequest = 421,
        UnprocessableEntity = 422,
        Locked = 423,
        FailedDependency = 424,
        TooEarly = 425,
        UpgradeRequired = 426,
        PreconditionRequired = 428,
        TooManyRequests = 429,
        RequestHeaderFieldsTooLarge = 431,
        RetryWith = 449,
        UnavailableForLegalReasons = 451,
        ClientClosedRequest = 499,

        InternalServerError = 500,
        NotImplemented = 501,
        BadGateway = 502,
        ServiceUnavailable = 503,
        GatewayTimeout = 504,
        HTTPVersionNotSupported = 505,
        VariantAlsoNegotitates = 506,
        InsufficientStorage = 507,
        LoopDetected = 508,
        BandwidthLimitExceeded = 509,
        NotExtended = 510,
        NetworkAuthenticationRequired = 511,
        UnknownError = 520,
        WebServerIsDown = 521,
        ConnectionTimedOut = 522,
        OriginIsUnreachable = 523,
        ATimeoutOccurred = 524,
        SSLHandshakeFailed = 525,
        InvalidSSLCertificate = 526,
    };
    Q_ENUM(Status)

    static Status statusFromInt(int value);

    HTTPResponse(Status status = Status::OK);

    void SetStatus(Status status);
    Status getStatus() const;
    int getStatusCode() const;
    QString getStatusName() const;

protected:
    virtual void parseStart(const QString& line) override;
    virtual void ParseBody(const QString &text) override;

    virtual QString startToString() override;

private:
    Status status;
    QString data;
};

#endif // REQUESTBODY_H
