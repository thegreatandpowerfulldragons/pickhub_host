#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class SettingsWindow; }
QT_END_NAMESPACE

class Settings;
class ShellRow;
class Application;

class SettingsWindow : public QMainWindow
{
    Q_OBJECT
public:
    SettingsWindow(Settings* settings);
    ~SettingsWindow();

protected:
    virtual void closeEvent(QCloseEvent* event) override;

public slots:
    void revert();
    void apply();

    void dssUpdated();

private:
    void addShell(QString extension, QString shell);
    bool hasShellExtension(QString extension, ShellRow* ignoreRow = nullptr);

    // Own objects
    Ui::SettingsWindow* ui;

    // External objects
    Settings* settings;
};

#endif // SETTINGSWINDOW_H
