#ifndef PREVIEWVIDEO_H
#define PREVIEWVIDEO_H

#include <QWidget>
#include "previewbase.h"
#include <QMediaPlayer>
#include <QVideoWidget>

namespace Ui {
    class PreviewVideo;
}

class PreviewVideo : public PreviewBase
{
    Q_OBJECT
public:
    explicit PreviewVideo(QWidget* parent = nullptr);
    ~PreviewVideo();

    virtual void open(const QString& filepath) override;

private slots:
    void togglePlay();

    void timeChanged(qint64 frame);
    void durationChanged(qint64 duration);
    void statusChanged(QMediaPlayer::MediaStatus status);
    void stateChanged(QMediaPlayer::State state);

    void timeDragStart();
    void timeDrag(int time);
    void timeDragEnd();

private:
    Ui::PreviewVideo* ui;
    QMediaPlayer* player;
    QVideoWidget* videoWidget;

    bool durationHours;
    bool stateBeforeTimeDrag;
};

#endif // PREVIEWVIDEO_H
