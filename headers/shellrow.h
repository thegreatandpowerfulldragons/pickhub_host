#ifndef SHELLROW_H
#define SHELLROW_H

#include <QWidget>
#include <QLineEdit>

class RowControl;

class ShellRow : public QWidget
{
    Q_OBJECT
public:
    explicit ShellRow(QString extension, QString shell, QWidget* parent = nullptr);
    ~ShellRow();

    QString getExtension() const;
    void setExtension(QString extension);

    QString getShell() const;
    void setShell(QString shell);

signals:
    void extensionChanged(QString oldValue, QString newValue);
    void shellChanged(QString oldValue, QString newValue);
    void remove();

private:
    QLineEdit* extensionField;
    QLineEdit* shellField;
    RowControl* rowControl;

    QString cachedExtension;
    QString cachedShell;
};

#endif // SHELLRECORD_H
