#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QJsonObject>
#include <QMap>
#include <QLocale>

class Application;

class Settings : public QObject
{
    Q_OBJECT
public:
    enum class Theme
    {
        Dark,
        Light
    };

    explicit Settings(Application* parent = nullptr);
    explicit Settings(QString path, Application* parent = nullptr);
    void load(const QString& path = "");
    void save(const QString& path = "");

    quint16 getPort() const;
    void setPort(quint16 value);

    Theme getTheme() const;
    void setTheme(Theme value);

    QString getLanguage() const;
    void setLanguage(QString value);

    const QMap<QString, QString>& getShells() const;
    void setShells(const QMap<QString, QString>& newSHells);
    void setShell(const QString& extension, const QString& shell);
    void changeShell(const QString& oldExtension, const QString& newExtension);
    void removeShell(const QString& extension);

    QJsonObject getDefault() const;

public slots:
    void reset();

signals:
    void portChanged();

    void shellAdded(const QString& extension, const QString& shell);
    void shellChanged(const QString& extension, const QString& shell);
    void shellRemoved(const QString& extension);

    void themeChanged();

    void languageChanged();

private:
    quint16 port                  = {};
    Theme theme                   = {};
    QString language              = {};
    QMap<QString, QString> shells = {};
};

#endif // SETTINGS_H
