#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <qtcpsocket.h>
#include <QNetworkReply>
#include <QIcon>

class Application;
class Settings;
class SaveRequest;

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject* parent = nullptr);
    ~Server();

    QString getShell(const QString& extension) const;

    bool findService(const QString& hostname, QString& shell, QString& fetcher) const;

    void syncCollections();

public slots:
    void slotNewConnection();
    void slotServerRead();
    void slotClientDisconnected();

    void stop();
    void restart();
    bool isStarted() const;

private:
    bool started;

    QTcpServer* tcp;
    QTcpSocket* socket;

    QVector<SaveRequest*> saveRequests;
};

#endif // SERVER_H
