#ifndef ROWCONTROL_H
#define ROWCONTROL_H

#include <QWidget>
#include <QToolButton>

class RowControl : public QWidget
{
    Q_OBJECT
public:
    explicit RowControl(bool canUp, bool canDown, bool canRemove, QWidget *parent = nullptr);
    ~RowControl();

    void setCanUp(bool state);
    void setCanDown(bool state);
    void setCanRemove(bool state);

public slots:
    void upClick();
    void downClick();
    void removeClick();

signals:
    void up();
    void down();
    void remove();

private:
    QToolButton* upButton;
    QToolButton* downButton;
    QToolButton* removeButton;
};

#endif // ROWCONTROL_H
