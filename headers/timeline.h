#ifndef TIMELINE_H
#define TIMELINE_H

#include <QSlider>
#include <QtCore>

class TimeLine : public QSlider
{
    Q_OBJECT
public:
    explicit TimeLine(QWidget* parent = nullptr);
    ~TimeLine();

protected:
    virtual void mousePressEvent(QMouseEvent* event) override;
};

#endif // TIMELINE_H
