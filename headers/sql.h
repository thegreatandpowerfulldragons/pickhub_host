#ifndef SQL_H
#define SQL_H
/*
#include <QSqlDatabase>
#include <QVariant>

class Version;
class Select;

class Alias
{
public:
    Alias(const QString& source);
    Alias(const QString& source, const QString& alias);

    QString toString() const;

private:
    QString source;
    QString alias;
};

class Field
{
public:
    static Field name(const QString& name);
    static Field value(const QVariant& value);

    QString toString(QSqlDriver* driver) const;

private:
    Field(bool isName, const QVariant& value);

    bool isName;
    QVariant val;
};

class Predicate
{
public:
    enum PredicateRelation
    {
        Larger = 0,
        LargerEquals = 1,
        Equals = 2,
        SmallerEquals = 3,
        Smaller = 4,
        NotEquals = 5
    };

    //static Predicate Relation(const Field& a, PredicateRelation relation, const Field& b);
    //static Predicate In(const Field& value, const Select& array);
    //static Predicate Between(const Field& value, const Field& from, const Field& to);

    void setDriver(QSqlDriver* driver);

    virtual QString toString() const = 0;

protected:
    Predicate();

    QSqlDriver* driver;
};

class Relation : public Predicate
{
public:
    Relation(const Field& a, PredicateRelation relation, const Field& b);

    QString toString() const override;

private:
    Field a;
    PredicateRelation relation;
    Field b;
};

class In : public Predicate
{
public:
    In(const Field& value, const Select& array);

    QString toString() const override;

private:
    Field value;
    Select array;
};

class Base
{
protected:
    Base(QSqlDatabase database);

    QSqlDatabase database;
    QString queryString;
};

class Version : public Base
{
public:
    Version(QSqlDatabase database);

    uint exec() const;
    void exec(uint value);
};

class Select : public Base
{
public:
    Select(QSqlDatabase database, const QVector<Alias>& aliases);

    Select& From(const QString& table);
    Select& Where(const Predicate& predicate);
    Select& Limit(uint pos);
    Select& Offset(uint off);

    const QString& toString() const;
};*/
#endif // SQL_H
