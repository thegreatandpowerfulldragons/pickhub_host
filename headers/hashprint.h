#ifndef HASHPRINT_H
#define HASHPRINT_H

#include <QtCore>
#include <QColor>

class HashPrint
{
public:
    HashPrint();
    HashPrint(const QPixmap& pixmap);
    HashPrint(const QByteArray& bytes);

    QByteArray toBytes() const;
    void toBytes(uchar* target[32]);
    QPixmap draw(int borderSize = 1, int cellSize = 6, const QColor& color = QColor(255, 255, 255));

    bool get(uchar x, uchar y) const;
    void set(uchar x, uchar y, bool value);

    bool operator ==(const HashPrint& rhs);

private:
    bool data[16][16];
};

#endif // HASHPRINT_H
