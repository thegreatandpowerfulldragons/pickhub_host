#ifndef PREVIEWBASE_H
#define PREVIEWBASE_H

#include <QWidget>

class PreviewBase : public QWidget
{
    Q_OBJECT
public:
    explicit PreviewBase(QWidget* parent = nullptr);

    virtual void open(const QString& filepath) = 0;

signals:

};

#endif // PREVIEWBASE_H
