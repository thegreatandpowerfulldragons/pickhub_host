#include "timeline.h"
#include <QPainter>
#include "application.h"
#include "dss.h"
#include <QMouseEvent>

TimeLine::TimeLine(QWidget* parent) :
    QSlider(parent)
{
    setPageStep(15);
}

TimeLine::~TimeLine()
{

}

void TimeLine::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
    {
        if (orientation() == Qt::Orientation::Horizontal)
        {
            setValue(minimum() + (event->pos().x() - pageStep() * 0.5) / (width() - pageStep()) * (maximum() - minimum()));
        }
        else
        {
            setValue(minimum() + (event->pos().y() - pageStep() * 0.5) / (height() - pageStep()) * (maximum() - minimum()));
        }

        event->accept();
    }

    QSlider::mousePressEvent(event);
}
