#include "sql.h"
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlField>
/*
Alias::Alias(const QString& source)
    : source(source)
    , alias()
{

}

Alias::Alias(const QString& source, const QString& alias)
    : source(source)
    , alias(alias)
{

}

QString Alias::toString() const
{
    if (alias.isEmpty())
    {
        return QString("'%1' AS '%2'").arg(source).arg(alias);
    }
    else
    {
        return QString("'%1'").arg(source);
    }
}

Field Field::name(const QString& name)
{
    return Field(true, name);
}

Field Field::value(const QVariant& value)
{
    return Field(false, value);
}

QString SQL::Field::toString(QSqlDriver* driver) const
{
    if (isName)
    {
        return val.toString();
    }
    else
    {
        QSqlField field("value", val.type());
        field.setValue(val);
        return driver->formatValue(field);
    }
}

Field::Field(bool isName, const QVariant& val)
    : isName(isName)
    , val(val)
{

}

Relation::Relation(const Field& a, Predicate::PredicateRelation relation, const Field& b)
    : Predicate()
    , a(a)
    , relation(relation)
    , b(b)
{

}namespace SQL
{

}

void Predicate::setDriver(QSqlDriver* driver)
{
    this->driver = driver;
}

Predicate::Predicate()
{

}

QString Relation::toString() const
{
    QStringList ops = { ">", ">=", "==", "<=", "<", "!=" };
    QString op = ops[relation];

    return QString("%1 %2 %3").arg(a.toString(driver)).arg(op).arg(b.toString(driver));
}

Version::Version(QSqlDatabase database)
    : Base(database)
{
    queryString += "PRAGMA user_version";
}

uint Version::exec() const
{
    QSqlQuery query(database);
    query.exec(queryString);
    query.next();
    return query.value("user_version").toUInt();
}

void Version::exec(uint value)
{
    QSqlField fieldValue("value", QVariant::Type::Int);
    fieldValue.setValue(value);
    queryString += " = " + database.driver()->formatValue(fieldValue);

    QSqlQuery query(database);
    query.exec(queryString);
}

Select::Select(QSqlDatabase database, const QVector<Alias>& aliases)
    : Base(database)
{
    queryString = "SELECT";

    for (const auto& alias : aliases)
    {
        queryString += " " + alias.toString();
    }
}

Select& Select::From(const QString& table)
{
    queryString += QString(" FROM %1").arg(table);

    return *this;
}

Select& Select::Where(const Predicate& predicate)
{
    queryString += QString(" WHERE %1").arg(predicate.toString());
}

const QString& Select::toString() const
{
    return queryString;
}*/
