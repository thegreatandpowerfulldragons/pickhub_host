#include "previewanimation.h"
#include "ui_previewanimation.h"
#include <QVideoWidgetControl>

PreviewAnimation::PreviewAnimation(QWidget* parent)
    : PreviewBase(parent)
    , ui(new Ui::PreviewAnimation)
{
    ui->setupUi(this);

    ui->graphics->setScene(new QGraphicsScene(this));

    movie = new QMovie(this);
    movie->setCacheMode(QMovie::CacheMode::CacheAll);

    connect(ui->playButton, SIGNAL(clicked()), this, SLOT(pauseToggle()));

    connect(ui->timeline, SIGNAL(sliderPressed()), this, SLOT(timeDragStart()));
    connect(ui->timeline, SIGNAL(sliderMoved(int)), this, SLOT(timeDrag(int)));
    connect(ui->timeline, SIGNAL(sliderReleased()), this, SLOT(timeDragEnd()));

    connect(movie, SIGNAL(stateChanged(QMovie::MovieState)), this, SLOT(playerStateChanged(QMovie::MovieState)));
    connect(movie, SIGNAL(frameChanged(int)), this, SLOT(playerFrameChanged(int)));
    connect(movie, SIGNAL(resized(const QSize)), this, SLOT(playerOriginResized(const QSize)));
    connect(movie, SIGNAL(error(QImageReader::ImageReaderError)), this, SLOT(playerError(QImageReader::ImageReaderError)));
}

PreviewAnimation::~PreviewAnimation()
{
    delete ui;
}

void PreviewAnimation::open(const QString& filepath)
{
    targetSize = QPixmap(filepath).size();

    movie->setFileName(filepath);
    movie->start();
    ui->timeline->setMaximum(movie->frameCount() - 1);
    ui->frameTotal->setText(QString::number(movie->frameCount()));

    updateSize();
}

void PreviewAnimation::pauseToggle()
{
    if (movie->isValid())
    {
        switch (movie->state())
        {
        case QMovie::MovieState::NotRunning:
            movie->start();
            break;
        case QMovie::MovieState::Running:
            movie->setPaused(true);
            break;
        case QMovie::MovieState::Paused:
            movie->setPaused(false);
            break;
        }
    }
}

void PreviewAnimation::setFrame(int frame)
{
    if (movie->isValid())
    {
        ui->timeline->setValue(frame);
        ui->frameCurrent->setText(QString::number(frame + 1));

        movie->jumpToFrame(frame);

        updateImage();
    }
}

void PreviewAnimation::resizeEvent(QResizeEvent*)
{
    if (movie->isValid())
    {
        updateSize();

        updateImage();
    }
}

void PreviewAnimation::updateImage()
{
    QPixmap scaled = movie->currentPixmap().scaled(ui->graphics->scene()->sceneRect().size().toSize(), Qt::AspectRatioMode::KeepAspectRatio);
    ui->graphics->scene()->clear();
    ui->graphics->scene()->addPixmap(scaled);
}

void PreviewAnimation::updateSize()
{
    QSize scaledSize = targetSize.scaled(ui->graphics->size(), Qt::AspectRatioMode::KeepAspectRatio);
    ui->graphics->scene()->setSceneRect(QRectF(QPointF(), scaledSize));
    movie->setScaledSize(scaledSize);
}

void PreviewAnimation::playerStateChanged(QMovie::MovieState state)
{
    ui->playButton->setText(QString::number((int)state));
}

void PreviewAnimation::playerFrameChanged(int frame)
{
    ui->timeline->setValue(frame);
    ui->frameCurrent->setText(QString::number(frame + 1));

    updateImage();
}

void PreviewAnimation::playerOriginResized(const QSize& size)
{

}

void PreviewAnimation::playerError(QImageReader::ImageReaderError error)
{

}

void PreviewAnimation::timeDragStart()
{
    stateBeforeTimeDrag = movie->state() == QMovie::MovieState::Running;

    if (stateBeforeTimeDrag)
    {
        movie->setPaused(true);
    }
}

void PreviewAnimation::timeDrag(int time)
{
    setFrame(time);
}

void PreviewAnimation::timeDragEnd()
{
    if (stateBeforeTimeDrag)
    {
        movie->start();
    }
}
