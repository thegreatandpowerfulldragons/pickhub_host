#include "rowcontrol.h"
#include <QHBoxLayout>

RowControl::RowControl(bool canUp, bool canDown, bool canRemove, QWidget* parent)
    : QWidget(parent)
    , upButton(nullptr)
    , downButton(nullptr)
    , removeButton(nullptr)
{    
    auto layout = new QHBoxLayout(this);
    layout->setMargin(0);
    setLayout(layout);

    upButton = new QToolButton(this);
    upButton->setText("▲");
    upButton->setVisible(canUp);
    layout->addWidget(upButton);
    connect(upButton, SIGNAL(clicked()), this, SLOT(upClick()));

    downButton = new QToolButton(this);
    downButton->setText("▼");
    downButton->setVisible(canDown);
    layout->addWidget(downButton);
    connect(downButton, SIGNAL(clicked()), this, SLOT(downClick()));

    removeButton = new QToolButton(this);
    removeButton->setText("➖");
    removeButton->setVisible(canRemove);
    layout->addWidget(removeButton);
    connect(removeButton, SIGNAL(clicked()), this, SLOT(removeClick()));

    setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));
}

RowControl::~RowControl()
{
    delete upButton;
    delete downButton;
    delete removeButton;
}

void RowControl::setCanUp(bool state)
{
    upButton->setVisible(state);
}

void RowControl::setCanDown(bool state)
{
    downButton->setVisible(state);
}

void RowControl::setCanRemove(bool state)
{
    removeButton->setVisible(state);
}

void RowControl::upClick()
{
    emit up();
}

void RowControl::downClick()
{
    emit down();
}

void RowControl::removeClick()
{
    emit remove();
}
