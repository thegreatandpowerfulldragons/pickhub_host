#include "server.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include "http.h"
#include <QDir>
#include <QProcess>
#include "application.h"
#include "settings.h"
#include "saverequest.h"
#include "settingswindow.h"

Server::Server(QObject* parent)
    : QObject(parent)
    , tcp(nullptr)
    , socket(nullptr)
{
    tcp = new QTcpServer(this);

    connect(tcp, &QTcpServer::newConnection, this, &Server::slotNewConnection);

    restart();
}

Server::~Server()
{
    delete tcp;
}

QString Server::getShell(const QString& extension) const
{
    auto& shells = Application::getSettings()->getShells();
    if (shells.contains(extension))
    {
        return shells[extension];
    }
    return "";
}

bool Server::findService(const QString& hostname, QString& shell, QString& fetcher) const
{
    QDir serviceDir(QDir::currentPath() + "/services/" + hostname);
    if (serviceDir.exists())
    {
        for (const QFileInfo& info : serviceDir.entryInfoList())
        {
            QString filename = info.fileName();
            if (info.isFile() && filename.mid(0, filename.lastIndexOf('.')) == "index")
            {
                shell = getShell(filename.contains('.') ? filename.mid(filename.lastIndexOf('.') + 1) : "");
                fetcher = info.filePath();
                return true;
            }
        }
    }
    else
    {
        QFile serviceRef(QDir::currentPath() + "/services/" + hostname);
        if (serviceRef.exists())
        {
            serviceRef.open(QFile::OpenModeFlag::ReadOnly);
            QByteArray refHost = serviceRef.readAll();
            serviceRef.close();
            return findService(refHost, shell, fetcher);
        }
    }

    return false;
}

void Server::syncCollections()
{
    for (auto saveRequest : saveRequests)
    {
        saveRequest->syncCollections();
    }
}

void Server::slotNewConnection()
{
    socket = tcp->nextPendingConnection();

    connect(socket, &QTcpSocket::readyRead, this, &Server::slotServerRead);
    connect(socket, &QTcpSocket::disconnected, this, &Server::slotClientDisconnected);
}

void Server::slotServerRead()
{
    while(socket->bytesAvailable() > 0)
    {
        HTTPRequest req;
        HTTPResponse res(HTTPResponse::Status::BadRequest);

        QByteArray bytes = socket->readAll();

        req.parseFrom(bytes);

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("content-type", "application/json; charset=utf-8");

        const QJsonObject& data = req.getData();

        if (req.getMethod() == HTTPRequest::Method::POST)
        {
            qDebug() << "Incoming request" << req.getPath()[0];

            if (req.getPath()[0] == "check")
            {
                res.SetStatus(HTTPResponse::Status::OK);
            }
            else if (req.getPath()[0] == "send")
            {
                if (data.contains("image"))
                {
                    SaveRequest* saveRequest = new SaveRequest(this);
                    saveRequests.push_back(saveRequest);
                    saveRequest->show();
                    saveRequest->raise();
                    saveRequest->beginLoading(data["image"].toString(), data["page"].toString());
                    connect(saveRequest, &SaveRequest::destroyed, [=](){
                        saveRequests.removeOne(saveRequest);
                        saveRequest->deleteLater();
                    });

                    res.SetStatus(HTTPResponse::Status::OK);
                }
                else if (data.contains("image_raw"))
                {
                    res.SetStatus(HTTPResponse::Status::NotImplemented);
                }
            }
        }

        socket->write(res.toString().toUtf8());
        socket->close();
    }
}

void Server::slotClientDisconnected()
{
    socket->close();
}

void Server::stop()
{
    tcp->close();

    Application::setupTrayRestart();
    qDebug() << "Server stoped";
    Application::setupTrayIconOffline();
}

void Server::restart()
{
    if (tcp->isListening())
    {
        stop();
    }

    quint16 port = Application::getSettings()->getPort();
    qDebug() << "Starting server on port" << port;
    if(!tcp->listen(QHostAddress::Any, port))
    {
        qDebug() << "Server not started:" << tcp->errorString();
        Application::setupTrayRestart();
        Application::setupTrayIconOffline();
    }
    else
    {
        qDebug() << "Server started";
        Application::setupTrayStop();
        Application::setupTrayIconOnline();
    }
}

bool Server::isStarted() const
{
    return tcp->isListening();
}
