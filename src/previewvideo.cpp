#include "previewvideo.h"
#include "ui_previewvideo.h"
#include <QGraphicsVideoItem>

PreviewVideo::PreviewVideo(QWidget* parent)
    : PreviewBase(parent)
    , ui(new Ui::PreviewVideo)
    , durationHours(false)
{
    ui->setupUi(this);
    ui->controls->hide();

    player = new QMediaPlayer(this);
    player->setNotifyInterval(10);

    videoWidget = new QVideoWidget(ui->graphics);
    ui->graphics->layout()->addWidget(videoWidget);
    player->setVideoOutput(videoWidget);

    connect(ui->playButton, SIGNAL(clicked()), this, SLOT(togglePlay()));

    connect(ui->timeline, SIGNAL(sliderPressed()), this, SLOT(timeDragStart()));
    connect(ui->timeline, SIGNAL(sliderMoved(int)), this, SLOT(timeDrag(int)));
    connect(ui->timeline, SIGNAL(sliderReleased()), this, SLOT(timeDragEnd()));

    connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(timeChanged(qint64)));
    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(durationChanged(qint64)));
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(statusChanged(QMediaPlayer::MediaStatus)));
    connect(player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(stateChanged(QMediaPlayer::State)));
}

PreviewVideo::~PreviewVideo()
{
    delete ui;
}

QString formatTime(quint64 value, bool showHours)
{
    uint miliseconds = value % 1000;
    uint seconds     = value / 1000 % 60;
    uint minutes     = value / 1000 / 60 % 60;
    uint hours       = value / 1000 / 60 / 60;

    QString result = QString("%1:%2").arg(minutes, 2, 10, QChar('0')).arg(seconds, 2, 10, QChar('0'));
    if (showHours) result = QString("%1:%2").arg(hours, 2, 10, QChar('0')).arg(result);

    return result;
}

void PreviewVideo::open(const QString& filepath)
{
    player->setMedia(QUrl::fromLocalFile(filepath));
}

void PreviewVideo::togglePlay()
{
    if (player->state() != QMediaPlayer::State::PlayingState)
    {
        player->play();
    }
    else
    {
        player->pause();
    }
}

void PreviewVideo::timeChanged(qint64 frame)
{
    ui->timeline->setValue(frame);
    ui->timeCurrent->setText(formatTime(frame, durationHours));
}

void PreviewVideo::durationChanged(qint64 duration)
{
    durationHours = duration >= 3600000;

    ui->timeline->setMaximum(duration);
    ui->timeTotal->setText(formatTime(duration, durationHours));
}

void PreviewVideo::statusChanged(QMediaPlayer::MediaStatus status)
{
    switch (status)
    {
    case QMediaPlayer::MediaStatus::LoadedMedia:
        ui->controls->show();
        player->play();
        player->pause();
        player->setPosition(0);
        break;
    case QMediaPlayer::MediaStatus::EndOfMedia:
        player->play();
        break;
    }
}

void PreviewVideo::stateChanged(QMediaPlayer::State state)
{
    ui->playButton->setText(QString::number(state));
}

void PreviewVideo::timeDragStart()
{
    stateBeforeTimeDrag = player->state() == QMediaPlayer::State::PlayingState;

    if (stateBeforeTimeDrag)
    {
        player->pause();
    }
}

void PreviewVideo::timeDrag(int time)
{
    player->setPosition(time);
}

void PreviewVideo::timeDragEnd()
{
    if (stateBeforeTimeDrag)
    {
        player->play();
    }
}
