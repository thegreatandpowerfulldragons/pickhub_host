#include "settings.h"
#include <QFile>
#include <QJsonDocument>
#include "application.h"
#include <QJsonArray>
#include "boolinq/include/boolinq/boolinq.h"

QJsonArray mergeJson(const QJsonArray& main, const QJsonArray& second)
{
    QJsonArray result;
    for (const auto& value : second)
    {
        if (!main.contains(value)) result += value;
    }
    return result;
}

QJsonObject mergeJson(const QJsonObject& main, const  QJsonObject& second)
{
    QJsonObject result = main;
    for (const auto& key : second.keys())
    {
        if (result[key].type() == QJsonValue::Type::Array && second[key].type() == QJsonValue::Type::Array)
        {
            result[key] = mergeJson(result[key].toArray(), second[key].toArray());
            continue;
        }
        else if (result[key].type() == QJsonValue::Type::Object && second[key].type() == QJsonValue::Type::Object)
        {
            result[key] = mergeJson(result[key].toObject(), second[key].toObject());
            continue;
        }
        result[key] = second[key];
    }

    return result;
}

template<typename kT, typename vT>
QJsonObject jsonFromMap(QMap<kT, vT> map)
{
    QJsonObject obj;
    for (const auto& key : map.keys())
    {
        obj[key] = map[key];
    }
    return obj;
}

Settings::Settings(Application* parent)
    : Settings("", parent)
{

}

Settings::Settings(QString path, Application* parent)
    : QObject(parent)
{
    load(path);
    save(path);
}

void Settings::load(const QString& path)
{
    QString filepath = path.isEmpty() ? "settings.json" : path;

    QJsonObject data = getDefault();

    QFile settingsFile(filepath);

    if (settingsFile.open(QFile::OpenMode(QFile::OpenModeFlag::ReadOnly)))
    {
        QJsonParseError error;
        QJsonDocument document = QJsonDocument::fromJson(settingsFile.readAll(), &error);

        if (error.error == QJsonParseError::NoError)
        {
            QJsonObject loaded = document.object();
            data = mergeJson(data, loaded);
        }
        settingsFile.close();
    }

    data["language"] = QLocale::system().bcp47Name();

    setPort(data["port"].toInt());
    setTheme((Theme)data["theme"].toInt());
    setLanguage(data["language"].toString());

    const auto shellsJson = data["shells"].toObject();
    QStringList oldShells = shells.keys();
    for (const auto extension : shellsJson.keys())
    {
        QString shell = shellsJson[extension].toString();
        if (shells.contains(extension))
        {
            shells[extension] = shell;
            emit shellChanged(extension, shell);
            oldShells.removeOne(shell);
        }
        else
        {
            shells[extension] = shell;
            emit shellAdded(extension, shell);
        }
    }
    for (const auto& oldShell : oldShells)
    {
        shells.remove(oldShell);
        emit shellRemoved(oldShell);
    }
}

void Settings::save(const QString& path)
{
    QString filepath = path.isEmpty() ? "settings.json" : path;

    QFile settingsFile(filepath);

    if (settingsFile.open(QFile::OpenMode(QFile::OpenModeFlag::WriteOnly)))
    {
        settingsFile.write(QJsonDocument({
            {"port", port},
            {"theme", (int)theme},
            {"language", language},
            {"shells", jsonFromMap(shells)}
        }).toJson());
        settingsFile.close();
    }
}

quint16 Settings::getPort() const
{
    return port;
}

void Settings::setPort(quint16 value)
{
    if (value != port)
    {
        port = value;
        emit portChanged();
    }
}

Settings::Theme Settings::getTheme() const
{
    return theme;
}

void Settings::setTheme(Settings::Theme value)
{
    if (theme != value)
    {
        theme = value;
        emit themeChanged();
    }
}

QString Settings::getLanguage() const
{
    return language;
}

void Settings::setLanguage(QString value)
{
    if (language != value)
    {
        language = value;
        emit languageChanged();
    }
}

const QMap<QString, QString>& Settings::getShells() const
{
    return shells;
}

void Settings::setShells(const QMap<QString, QString>& newshells)
{
    auto oldKeys = shells.keys();
    for (const auto& key : newshells.keys())
    {
        if (shells.contains(key))
        {
            shells[key] = newshells[key];
            emit shellChanged(key, newshells[key]);
            oldKeys.removeOne(key);
        }
        else
        {
            shells[key] = newshells[key];
            emit shellAdded(key, newshells[key]);
        }
    }
    for (const auto& key : oldKeys)
    {
        shells.remove(key);
        emit shellRemoved(key);
    }
}

void Settings::setShell(const QString& extension, const QString& shell)
{
    if (shell.contains(extension))
    {
        shells[extension] = shell;
        emit shellChanged(extension, shell);
    }
    else if (shells[extension] != shell)
    {
        shells[extension] = shell;
        emit shellAdded(extension, shell);
    }
}

void Settings::changeShell(const QString& oldExtension, const QString& newExtension)
{
    if (shells.contains(oldExtension))
    {
        shells[newExtension] = shells[oldExtension];
        shells.remove(oldExtension);
        emit shellRemoved(oldExtension);
        emit shellAdded(newExtension, shells[newExtension]);
    }
}

void Settings::removeShell(const QString& extension)
{
    shells.remove(extension);
    emit shellRemoved(extension);
}

QJsonObject Settings::getDefault() const
{
    QFile file(":/settings.json");
    file.open(QFile::OpenModeFlag::ReadOnly);
    QJsonObject data = QJsonDocument::fromJson(file.readAll()).object();
    data["language"] = QLocale::system().bcp47Name();
    file.close();
    return data;
}

void Settings::reset()
{
    load(".");
}
