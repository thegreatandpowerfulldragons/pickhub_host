#include "shellrow.h"
#include <QHBoxLayout>
#include "rowcontrol.h"

ShellRow::ShellRow(QString extension, QString executor, QWidget* parent)
    : QWidget(parent)
    , extensionField(nullptr)
    , shellField(nullptr)
    , rowControl(nullptr)
    , cachedExtension(extension)
    , cachedShell(executor)
{
    auto layout = new QHBoxLayout();
    layout->setMargin(0);
    setLayout(layout);

    extensionField = new QLineEdit(cachedExtension, this);
    extensionField->sizePolicy().setHorizontalStretch(1);
    layout->addWidget(extensionField);

    shellField = new QLineEdit(cachedShell, this);
    shellField->sizePolicy().setHorizontalStretch(2);
    layout->addWidget(shellField);

    rowControl = new RowControl(false, false, true, this);
    layout->addWidget(rowControl);

    setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum));

    connect(extensionField, &QLineEdit::editingFinished, [=](){
        if (extensionField->text() != cachedExtension)
        {
            if (extensionField->text() == "")
            {
                extensionField->setText(cachedExtension);
            }
            else
            {
                emit extensionChanged(cachedExtension, extensionField->text());
            }
            cachedExtension = extensionField->text();
        }
    });
    connect(shellField, &QLineEdit::editingFinished, [=](){
        if (shellField->text() != cachedShell)
        {
            emit shellChanged(cachedShell, shellField->text());
            cachedShell = shellField->text();
        }
    });
    connect(rowControl, &RowControl::remove, [=](){
        emit remove();
    });
}

ShellRow::~ShellRow()
{
    delete extensionField;
    delete shellField;
    delete rowControl;
}

QString ShellRow::getExtension() const
{
    return extensionField->text();
}

void ShellRow::setExtension(QString extension)
{
    extensionField->setText(extension);
}

QString ShellRow::getShell() const
{
    return shellField->text();
}

void ShellRow::setShell(QString shell)
{
    shellField->setText(shell);
}
