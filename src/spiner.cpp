#include "spiner.h"
#include "ui_spiner.h"
#include <QTimer>
#include <QPainter>

Spiner::Spiner(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::Spiner)
    , counter(0)
{
    ui->setupUi(this);

    setMinimumSize({50, 50});
    setMaximumSize({50, 50});

    QTimer* timer = new QTimer(this);
    timer->setInterval(5);

    connect(timer, &QTimer::timeout, this, [=](){
        tick();
    });
}

Spiner::~Spiner()
{
    delete ui;
}

void Spiner::paintEvent(QPaintEvent*)
{
    QPainter painter(this);

    painter.drawArc(rect(), 000 + counter * 0.1, 60);
    painter.drawArc(rect(), 120 + counter * 0.1, 60);
    painter.drawArc(rect(), 240 + counter * 0.1, 60);
}

void Spiner::tick()
{
    counter++;
    update();
}
