#include "http.h"

QString camelCaseToNormal(QString camelCase)
{
    return camelCase.replace(QRegExp("([A-Z])([A-Z][a-z])"), "$1 $2").replace(QRegExp("([a-z])([A-Z])"), "$1 $2");
}

enum class EHttpParsingStage {
    Start,
    Headers,
    Body
};

void HTTP::parseFrom(const QString& input)
{
    EHttpParsingStage stage = EHttpParsingStage::Start;

    QStringList lines = input.split("\r\n");
    for (int i = 0; i < lines.length(); i++)
    {
        QString line = lines[i];

        switch (stage)
        {
        case EHttpParsingStage::Start:
        {
            parseStart(line);
            stage = EHttpParsingStage::Headers;
        }
        break;

        case EHttpParsingStage::Headers:
        {
            if (line.length() > 0)
            {
                ParseHeader(line);
            }
            else
            {
                stage = EHttpParsingStage::Body;
            }
        }
        break;

        case EHttpParsingStage::Body:
        {
            for (i = i + 1; i < lines.length(); i++)
            {
                line += "\r\n" + lines[i];
            }
            body = line;
            ParseBody(line);
        }
        break;
        }
    }
}

const QMap<QString, QString>& HTTP::getHeaders() const
{
    return headers;
}

QString HTTP::getHeader(const QString& name)
{
    return headers.contains(name) ? headers[name] : "";
}

void HTTP::setHeader(const QString& name, const QString& value)
{
    headers[name] = value;
}

void HTTP::setBody(const QString& value)
{
    body = value;
}

const QString& HTTP::getBody()
{
    return body;
}

QString HTTP::toString()
{
    QString result;

    result += startToString() + "\r\n";

    for (const auto& header : headers)
    {
        result += header + ": " + headers[header] + "\r\n";
    }

    result += "\r\n";

    result + body;

    return result;
}

void HTTP::ParseHeader(const QString &line)
{
    int divider = line.indexOf(':');
    QString name = line.mid(0, divider);
    QString value = line.mid(divider + 2);
    headers[name] = value;
}

void HTTPRequest::setMethod(HTTPRequest::Method value)
{
    method = value;
}

HTTPRequest::Method HTTPRequest::getMethod() const
{
    return method;
}

QString HTTPRequest::getMethodName() const
{
    return camelCaseToNormal(QString(QMetaEnum::fromType<Method>().key(static_cast<int>(method))));
}

void HTTPRequest::setVersion(double value)
{
    version = value;
}

double HTTPRequest::getVersion() const
{
    return version;
}

void HTTPRequest::setPath(const QStringList& value)
{
    path = value;
}

const QStringList& HTTPRequest::getPath() const
{
    return path;
}

const QJsonObject& HTTPRequest::getData()
{
    return data;
}

void HTTPRequest::parseStart(const QString &line)
{
    QStringList parts = line.split(' ');

    method = Method(QMetaEnum::fromType<Method>().keyToValue(parts[0].toLocal8Bit().data()));
    version = parts[2].mid(parts[2].indexOf('/') + 1).toDouble();

    QStringList url = parts[1].split('?');

    path = url[0].split("/", QString::SplitBehavior::SkipEmptyParts);

    if (method == Method::GET)
    {
        if (url.length() > 1)
        {
            QStringList params = url[1].split('&');
            for (QString param : params)
            {
                int divider = param.indexOf('=');
                QString name = param.mid(0, divider);
                QString value = param.mid(divider + 1);

                data[name] = value;
            }
        }
        else
        {
            data = QJsonObject();
        }
    }
}

void HTTPRequest::ParseBody(const QString &text)
{
    tryParseBodyToData();
}

QString HTTPRequest::startToString()
{
    return getMethodName() + " " + getPath().join(" ") + " " + "HTTP/" + QString::number(getVersion());
}

void HTTPRequest::tryParseBodyToData()
{
    if (method == Method::POST/* && getHeader("Content-Type") == "application/json"*/)
    {
        QJsonParseError error;
        QJsonObject object = QJsonDocument::fromJson(getBody().toUtf8(), &error).object();
        if (error.error == QJsonParseError::NoError)
        {
            data = object;
        }
        else
        {
            data = QJsonObject();
        }
    }
}

HTTPResponse::HTTPResponse(HTTPResponse::Status statusCode)
    : HTTP()
    , status(statusCode)
{

}

void HTTPResponse::SetStatus(Status statusCode)
{
    this->status = statusCode;
}

HTTPResponse::Status HTTPResponse::getStatus() const
{
    return status;
}

int HTTPResponse::getStatusCode() const
{
    return static_cast<int>(status);
}

QString HTTPResponse::getStatusName() const
{
    return camelCaseToNormal(QMetaEnum::fromType<Status>().key(static_cast<int>(status)));
}

void HTTPResponse::parseStart(const QString& line)
{
    QStringList parts = line.split(' ');
    status = Status(parts[1].toInt());
}

void HTTPResponse::ParseBody(const QString& text)
{
    data = text;
}

QString HTTPResponse::startToString()
{
    return "HTTP/1.1 " + QString::number(getStatusCode()) + " " + getStatusName();
}
