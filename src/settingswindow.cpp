#include "settingswindow.h"
#include "ui_settingswindow.h"
#include "settings.h"
#include "shellrow.h"
#include <QSpinBox>
#include <QObject>
#include "application.h"
#include "dss.h"

SettingsWindow::SettingsWindow(Settings* settings)
    : QMainWindow(nullptr)
    , ui(new Ui::SettingsWindow)
    , settings(settings)
{
    ui->setupUi(this);

    dssUpdated();

    revert();

    connect(ui->addShell, &QPushButton::clicked, [=](){
        uint i = 1;
        while (hasShellExtension("ext" + QString::number(i)))
        {
            i++;
        }
        addShell("ext" + QString::number(i), "");
    });
    connect(ui->resetButton, SIGNAL(clicked()), settings, SLOT(reset()));
    connect(ui->revertButton, SIGNAL(clicked()), this, SLOT(revert()));
    connect(ui->applyButton, SIGNAL(clicked()), this, SLOT(apply()));
    connect(Application::getDss(), SIGNAL(updated()), this, SLOT(dssUpdated()));
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}

void SettingsWindow::closeEvent(QCloseEvent*)
{
    deleteLater();
}

void SettingsWindow::revert()
{
    ui->portEdit->setValue(settings->getPort());

    while (ui->shellsList->layout()->count() != 0)
    {
        delete ui->shellsList->layout()->itemAt(0)->widget();
    }
    const auto& shells = settings->getShells();
    for (const auto& shell : shells.keys())
    {
        addShell(shell, shells[shell]);
    }
}

void SettingsWindow::apply()
{
    if (settings->getPort() != ui->portEdit->value())
    {
        settings->setPort(ui->portEdit->value());
    }

    QMap<QString, QString> shells;
    for (int i = 0; i < ui->shellsList->layout()->count(); i++)
    {
        auto record = dynamic_cast<ShellRow*>(ui->shellsList->layout()->itemAt(i)->widget());
        shells[record->getExtension()] = record->getShell();
    }
    settings->setShells(shells);

    if ((int)settings->getTheme() != ui->themeEdit->currentIndex())
    {
        settings->setTheme((Settings::Theme)ui->themeEdit->currentIndex());
    }

    settings->save();
}

void SettingsWindow::dssUpdated()
{
    setStyleSheet(Application::getDss()->toString());
}

void SettingsWindow::addShell(QString extension, QString shell)
{
    ShellRow* recordWidget = new ShellRow(extension, shell, this);
    ui->shellsList->layout()->addWidget(recordWidget);

    connect(recordWidget, &ShellRow::extensionChanged, [=](QString oldValue, QString newValue){
        if (hasShellExtension(newValue, recordWidget))
        {
            recordWidget->setExtension(oldValue);
        }
    });

    connect(recordWidget, &ShellRow::remove, [=](){
        delete recordWidget;
    });
}

bool SettingsWindow::hasShellExtension(QString extension, ShellRow* ignoreRow)
{
    for (int i = 0; i < ui->shellsList->layout()->count(); i++)
    {
        ShellRow* row = dynamic_cast<ShellRow*>(ui->shellsList->layout()->itemAt(i)->widget());

        if (row == ignoreRow) continue;

        if (row->getExtension() == extension) return true;
    }
    return false;
}
