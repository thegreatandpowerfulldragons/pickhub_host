#include "previewimage.h"
#include "ui_previewimage.h"
#include <QResizeEvent>

QPointF sizeToPoint(const QSizeF& size)
{
    return QPointF(size.width(), size.height());
}

PreviewImage::PreviewImage(QWidget *parent)
    : PreviewBase(parent)
    , ui(new Ui::PreviewImage)
{
    ui->setupUi(this);

    ui->graphics->setScene(new QGraphicsScene(this));
}

PreviewImage::~PreviewImage()
{
    delete ui;
}

void PreviewImage::open(const QString& filepath)
{
    pixmap.load(filepath);
    redraw();
}

void PreviewImage::resizeEvent(QResizeEvent*)
{
    if (!pixmap.isNull())
    {
        redraw();
    }
}

void PreviewImage::redraw()
{
    QPixmap scaled = pixmap.scaled(size(), Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);
    ui->graphics->setSceneRect(QRectF(QPointF(), scaled.size()));
    ui->graphics->scene()->clear();
    ui->graphics->scene()->addPixmap(scaled);
}
