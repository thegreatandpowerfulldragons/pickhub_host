#include "browser.h"
#include "ui_browser.h"
#include "collection.h"
#include <QFileInfo>
#include "application.h"
#include <QPushButton>
#include <QDir>
#include <QStandardItemModel>
#include <QDesktopServices>
#include <QKeyEvent>
#include "dss.h"
#include <QMessageBox>
#include "hashprint.h"
#include "view.h"
#include <QMovie>
#include <QSlider>

bool commandAvailable(const QString& command)
{
    QProcess proc;
    proc.start("which", { command }, QProcess::OpenModeFlag::ReadOnly);
    return proc.waitForFinished() && !proc.readAll().isEmpty();
}

QStringList managers = {
    "nautilus",  //     tested
    "konqueror", // not tested
    "dolphin",   // not tested
    "krusader"   // not tested
};

Browser::Browser(Collection* collection)
    : QMainWindow(nullptr)
    , ui(new Ui::Browser)
    , collection(collection)
{
    ui->setupUi(this);

    dssUpdated();

    setWindowTitle("PickHub - " + collection->getName());

    ui->infoSourceValue->setIcon("\uf35d");

#if defined(Q_OS_WINDOWS)

#elif defined (Q_OS_UNIX)
    ui->itemLocate->setVisible(false);
    for (const auto& manager : managers)
    {
        if (commandAvailable(manager))
        {
            ui->itemLocate->setVisible(true);
            break;
        }
    }
#endif

    initItems();

    connect(ui->infoSourceValue, &ClickableLabel::clicked, [=](){
        QDesktopServices::openUrl(selectedItem.page);
    });
    connect(ui->items, &ItemView::itemSelected, [=](int index){
        if (index > 0)
        {
            selectItem(index);
        }
        else
        {
            selectItem(0);
        }
    });
    connect(ui->items, &ItemView::itemOpen, [=](int index){
        if (index > 0)
        {
            openItem(index);
        }
    });
    connect(ui->items, &ItemView::itemLocate, [=](int index){
        if (index > 0)
        {
            locateItem(index);
        }
    });
    connect(ui->items, &ItemView::itemDelete, [=](int index){
        if (index > 0)
        {
            deleteItem(index);
        }
    });
    connect(ui->searchInput, SIGNAL(returnPressed()), SLOT(searchItems()));
    connect(ui->searchButton, SIGNAL(clicked()), SLOT(searchItems()));
    connect(ui->searchClear, SIGNAL(clicked()), SLOT(clearSearch()));
    connect(ui->itemLocate, &QToolButton::clicked, [=](){
        locateItem(selectedItem.id);
    });
    connect(ui->itemOpen, &QToolButton::clicked, [=](){
        openItem(selectedItem.id);
    });
    connect(ui->itemDelete, &QToolButton::clicked,[=](){
        deleteItem(selectedItem.id);
    });
    connect(collection, SIGNAL(itemAdded(Collection::Item)), this, SLOT(itemAdded(Collection::Item)));
    connect(collection, SIGNAL(itemRemoved(uint)), this, SLOT(itemRemoved(uint)));
    connect(collection, SIGNAL(destroyed()), this, SLOT(close()));
    connect(Application::getDss(), SIGNAL(updated()), this, SLOT(dssUpdated()));
}

Browser::~Browser()
{
    delete ui;
}

QSize Browser::getItemSize() const
{
    return QSize(ui->items->getItemSize(), ui->items->getItemSize());
}

void Browser::selectItem(uint id)
{
    ui->infoHash->setVisible(id > 0);
    ui->infoTime->setVisible(id > 0);
    ui->infoSource->setVisible(id > 0);
    ui->tagsScrollView->setVisible(id > 0);
    ui->itemLocate->setVisible(id > 0);
    ui->itemOpen->setVisible(id > 0);
    ui->itemDelete->setVisible(id > 0);

    if (id > 0 && collection->getItem(id, selectedItem))
    {
        ui->infoHash->setVisible(selectedItem.hash != nullptr);
        if (selectedItem.hash != nullptr)
        {
            ui->infoHashValue->setPixmap(selectedItem.hash->draw());
        }
        ui->infoTimeValue->setText(selectedItem.time.toString("dd.MM.yy hh:mm:ss"));
        ui->infoSourceValue->setText(QUrl(selectedItem.page).host());
        ui->infoSourceValue->setToolTip(selectedItem.page);

        while (ui->tagsList->layout()->count() > 0)
        {
            delete ui->tagsList->layout()->itemAt(0)->widget();
        }
        for (const QString& tag : selectedItem.tags)
        {
            QPushButton* button = new QPushButton(tag);
            ui->tagsList->layout()->addWidget(button);

            connect(button, &QPushButton::clicked, [=](){
                ui->searchInput->setText(tag);
                searchItems();
            });
        }
    }
}

void Browser::itemAdded(const Collection::Item& item)
{
    ui->items->addThumbnail(item.id, { getThumbnail(item) , item.type}, true);
}

void Browser::itemRemoved(uint id)
{
    if (selectedItem.id == id)
    {
        selectItem(0);
    }

    ui->items->removeThumbnail(id);
}

void Browser::initItems()
{
    selectItem(0);

    QMap<int, ItemView::Item> thumbnails;
    QVector<Collection::ItemShort> items;
    if (collection->getAllItems(items))
    {
        for (const auto& item : items)
        {
            thumbnails[item.id] = { getThumbnail(item), item.type };
        }
    }

    ui->items->setupThumbnails(thumbnails);
}

void Browser::searchItems()
{
    QVector<int> indexes;
    QVector<Collection::ItemShort> items;
    if (collection->searchItems(items, ui->searchInput->text()))
    {
        for (const auto& item : items)
        {
            indexes += item.id;
        }
    }

    ui->items->DisplaySpecific(indexes);
}

void Browser::clearSearch()
{
    ui->searchInput->clear();
    ui->items->displayAll();
}

void Browser::dssUpdated()
{
    setStyleSheet(Application::getDss()->toString());
}

void Browser::closeEvent(QCloseEvent*)
{
    deleteLater();
}

void Browser::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
    case Qt::Key::Key_F:
        if ((event->modifiers() & Qt::KeyboardModifier::KeyboardSCAMask) == Qt::KeyboardModifier::ControlModifier)
        {
            ui->searchInput->setFocus(Qt::FocusReason::ShortcutFocusReason);
        }
        break;

    case Qt::Key::Key_Escape:
        if (ui->items->getDisplayMode() == ItemView::DisplayMode::Specific)
        {
            ui->searchInput->clear();
            ui->items->displayAll();
        }
        break;

    default:
        event->ignore();
        break;
    }
}

QPixmap Browser::getThumbnail(const Collection::ItemShort& item)
{
    QString itemPath = collection->paths.getPath() + "/" + QString::number(item.id);

    switch (item.type)
    {
        case Collection::ItemType::Image:
        {
            return QPixmap(itemPath).scaled(getItemSize(), Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);
        }

        case Collection::ItemType::Animation:
        {
            QMovie movie(itemPath);
            movie.setCacheMode(QMovie::CacheMode::CacheAll);
            movie.jumpToFrame(movie.frameCount() / 2);
            return movie.currentPixmap().scaled(getItemSize(), Qt::AspectRatioMode::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation);
        }

        default:
        {
            return QPixmap();
        }
    }
}

void Browser::locateItem(int index)
{
#if defined(Q_OS_WINDOWS)
    QProcess().start("explorer", { QString("%1/%2").arg(collection->paths.getPath()).arg(index) });
#elif defined (Q_OS_UNIX)
    for (const auto& manager : managers)
    {
        if (commandAvailable(manager))
        {
            QProcess* managerProcess = new QProcess();
            managerProcess->start(manager, { QString("%1/%2").arg(collection->paths.getPath()).arg(index) });
            break;
        }
    }
#endif
}

void Browser::openItem(int index)
{
    View* view = new View(collection, index);
    view->showMaximized();
    view->raise();
}

void Browser::deleteItem(int index)
{
    QMessageBox mb(QMessageBox::Icon::Question, tr("Warning"), tr("Are you sure you want to delete this element?"), QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No);
    if (mb.exec() & QMessageBox::StandardButton::Yes)
    {
        collection->remove(index);
    }
}
