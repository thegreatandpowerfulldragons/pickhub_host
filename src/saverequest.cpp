#include "saverequest.h"
#include "ui_saverequest.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include "application.h"
#include "server.h"
#include <QMetaEnum>
#include <QResizeEvent>
#include <QDir>
#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include "dss.h"
#include "previewimage.h"
#include "previewanimation.h"
#include "previewvideo.h"

QString SaveRequest::latestCollection = "";

SaveRequest::SaveRequest(Server* server)
    : QMainWindow(nullptr)
    , ui(new Ui::SaveRequest)
    , commitKey(-1)
    , tagLoader(nullptr)
    , manager(nullptr)
    , reply(nullptr)
    , preview(nullptr)
    , server(server)
    , loaded(false)
    , closingDelayed(false)
{
    ui->setupUi(this);

    dssUpdated();

    ui->commitProgress->setVisible(false);

    createField();

    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(confirm()));
    connect(ui->collections, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int){
        updateStatus();
    });
    connect(Application::getDss(), SIGNAL(updated()), this, SLOT(dssUpdated()));

    syncCollections();

    updateStatus();
}

SaveRequest::~SaveRequest()
{
    delete tagLoader;

    if (manager != nullptr) manager->deleteLater();

    if (!filename.isEmpty()) QFile(filename).remove();
}

void SaveRequest::beginLoading(const QString& originUrl, const QString& pageUrl)
{
    origin = originUrl;
    page = pageUrl;

    setWindowTitle(tr("Save from %1").arg(origin.host()));

    ui->pageLabel->setVisible(!page.isEmpty());

    ui->pageLabel->setText(tr("page:") + " " + page.toString());
    ui->sourceLabel->setText(tr("origin:") + " " + origin.toString());

    setupLoading();
    setProgress(0, 0);

    manager = new QNetworkAccessManager(this);

    QNetworkRequest request;
    request.setUrl(origin);
    request.setRawHeader("User-Agent", "PickHubHost/1.0");
    reply = manager->get(request);

    connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(loadingProgress(qint64, qint64)));
    connect(reply, SIGNAL(readyRead()), this, SLOT(loadingReadyRead()));
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(loadingFinished(QNetworkReply*)));

    QString shell;
    QString fetcher;
    if (server->findService(page.host(), shell, fetcher))
    {
        QStringList processParams = { fetcher };
        processParams.append({ "-origin", origin.toString() });
        if (!page.isEmpty()) processParams.append({ "-page", page.toString() });
        tagLoader = new QProcess(this);
        connect(tagLoader, SIGNAL(finished(int)), this, SLOT(tagsLoaded(int)));
        tagLoader->start(shell, processParams);
    }
}

void SaveRequest::syncCollections()
{
    QString previous = collections.empty() ? "" : collections.keys()[ui->collections->currentIndex()];

    collections.clear();
    for (const auto& collection : Application::getCollectionsPaths())
    {
        collections[collection] = QFileInfo(collection).baseName();
    }

    ui->collections->clear();
    ui->collections->addItems(collections.values());

    if (!previous.isEmpty())
    {
        ui->collections->setCurrentIndex(collections.keys().indexOf(previous));
    }
    else
    {
        ui->collections->setCurrentIndex(collections.keys().indexOf(latestCollection));
    }
}

void SaveRequest::loadingProgress(qint64 value, qint64 total)
{
    setProgress(value, total);
}

void SaveRequest::loadingReadyRead()
{
    while (reply->bytesAvailable() > 0)
    {
        loadBytes.push_back(reply->readAll());
    }
}

void SaveRequest::loadingFinished(QNetworkReply* reply)
{
    QString type = reply->header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString();
    QString mimeName = type.split('/')[0];
    QString mimeSubname = type.split('/')[1];

    if (mimeName == "image")
    {
        if (mimeSubname == "gif")
        {
            resourceType = Collection::ItemType::Animation;
        }
        else
        {
            resourceType = Collection::ItemType::Image;
        }
    }
    else if (mimeName == "video")
    {
        resourceType = Collection::ItemType::Video;
    }
    else
    {
        setupError(tr("Unknown resource type %1").arg(type));
    }

    if (reply->error() != QNetworkReply::NetworkError::NoError)
    {
        setupError(QMetaEnum::fromType<QNetworkReply::NetworkError>().valueToKey(reply->error()));
    }
    else
    {
        filename = dynamic_cast<Application*>(server->parent())->applicationDirPath() + "/.temp/" + QUrl::toPercentEncoding(reply->url().toString()) + "." + mimeSubname;
        QFile temp(filename);
        if (temp.open(QFile::OpenModeFlag::WriteOnly))
        {
            temp.write(loadBytes);
            temp.close();
            loadBytes.clear();

            setupLoaded();
        }
        else
        {
            setupError(tr("Could not save temp data into %1").arg(filename));
        }
    }
}

void SaveRequest::tagsLoaded(int code)
{
    QString message = QString(tagLoader->readAll());

    if (code == 0)
    {
        QStringList loadedTags = message.split(' ', QString::SplitBehavior::SkipEmptyParts);

        for (QString loadedTag : loadedTags)
        {
            loadedTag = loadedTag.trimmed();
            if (!loadedTag.isEmpty())
            {
                leadingTagField->setText(loadedTag);
                createField();
            }
        }
    }
    else
    {
        qDebug() << "tag loader for host" << origin.host() << "finished with code" << code << endl << message;
    }
}

void SaveRequest::confirm()
{
    latestCollection = collections.keys()[ui->collections->currentIndex()];

    QStringList tags;
    for (int i = 0; i < ui->tagsList->layout()->count(); i++)
    {
        auto field = qobject_cast<QLineEdit*>(ui->tagsList->layout()->itemAt(i)->widget());
        if (field != leadingTagField) tags.push_back(field->text());
    }

    Collection* collection = Collection::get(getCurrentCollectionPath());

    connect(collection, &Collection::commitFinished, this, [=](int key, bool state){
        if (commitKey == key)
        {
            ui->commitProgress->setVisible(false);

            commitKey = -1;

            updateStatus();

            if (state || closingDelayed)
            {
                close();
            }
        }
    });
    connect(collection, &Collection::commitProgress, this, [=](int key, uint value, uint total, QString comment){
        if (commitKey == key)
        {
            ui->commitProgress->setVisible(true);

            ui->commitProgress->setValue(value);
            ui->commitProgress->setMaximum(total);
            ui->commitProgress->setFormat(comment);
        }
    });
    commitKey = collection->commit(filename, origin, page, tags, resourceType);

    updateStatus();
}

void SaveRequest::cancel()
{
    if (commitKey >= 0)
    {
        Collection::get(getCurrentCollectionPath())->cancelCommit(commitKey);
    }
    else
    {
        close();
    }
}

void SaveRequest::dssUpdated()
{
    setStyleSheet(Application::getDss()->toString());
}

void SaveRequest::closeEvent(QCloseEvent* event)
{
    if (commitKey >= 0)
    {
        closingDelayed = true;
        Collection::get(getCurrentCollectionPath())->cancelCommit(commitKey);
        event->ignore();
    }
    else
    {
        deleteLater();
    }
}

void SaveRequest::createField(const QString& content)
{
    QLineEdit* field = new QLineEdit(content);
    QString* cache = new QString();

    leadingTagField = field;

    ui->tagsList->layout()->addWidget(field);
    ui->tagsList->layout()->addWidget(leadingTagField);

    connect(field, &QLineEdit::editingFinished, [=](){
        if (field != leadingTagField)
        {
            if (field->text() == "")
            {
                field->deleteLater();
                delete cache;
            }
            else if (hasTag(field->text(), field))
            {
                field->setText(*cache);
            }
            else
            {
                *cache = field->text();
            }
        }
        else
        {
            if (field->text() != "")
            {
                if (hasTag(field->text(), field))
                {
                    field->setText(*cache);
                }
                else
                {
                    *cache = field->text();
                    createField();
                }
            }
        }
    });
}

bool SaveRequest::hasTag(const QString& tag, QLineEdit* fieldToIgnore)
{
    for (int i = 0; i < ui->tagsList->layout()->count(); i++)
    {
        auto field = qobject_cast<QLineEdit*>(ui->tagsList->layout()->itemAt(i)->widget());
        if (field != fieldToIgnore && field->text() == tag) return true;
    }
    return false;
}

void SaveRequest::setupLoading()
{
    ui->resourceProgress->setVisible(true);
    ui->resourceView->setVisible(false);
    ui->resourceError->setVisible(false);
}

void SaveRequest::setupError(const QString& error)
{
    qDebug() << error;

    ui->resourceProgress->setVisible(false);
    ui->resourceView->setVisible(false);
    ui->resourceError->setVisible(true);

    ui->resourceError->setText(error);
}

void SaveRequest::setupLoaded()
{
    ui->resourceProgress->setVisible(false);
    ui->resourceView->setVisible(true);
    ui->resourceError->setVisible(false);

    if (preview != nullptr)
    {
        delete preview;
        preview = nullptr;
    }
    switch (resourceType)
    {
    case Collection::ItemType::Image:
        preview = new PreviewImage(this);
        break;

    case Collection::ItemType::Animation:
        preview = new PreviewAnimation(this);
        break;

    case Collection::ItemType::Video:
        preview = new PreviewVideo(this);
        break;

    case Collection::ItemType::Audio:
        throw "Video not implemented";
        break;
    }
    if (preview != nullptr)
    {
        preview->open(filename);
        ui->resourceView->layout()->addWidget(preview);
        preview->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding));
    }

    loaded = true;

    updateStatus();
}

void SaveRequest::setProgress(quint64 value, quint64 total)
{
    QString unit;
    double valueTruncated;
    double totalTruncated;

    if (total >= 1099511627776)
    {
        valueTruncated = value / 1099511627776.;
        totalTruncated = total / 1099511627776.;
        unit = "Tb";
    }
    else if(total >= 1073741824)
    {
        valueTruncated = value / 1073741824.;
        totalTruncated = total / 1073741824.;
        unit = "Gb";
    }
    else if (total >= 1048576)
    {
        valueTruncated = value / 1048576.;
        totalTruncated = total / 1048576.;
        unit = "Mb";
    }
    else if (total >= 1024)
    {
        valueTruncated = value / 1024.;
        totalTruncated = total / 1024.;
        unit = "Kb";
    }
    else
    {
        valueTruncated = value;
        totalTruncated = total;
        unit = "b";
    }

    ui->resourceProgress->setFormat(QString("%1 / %2 %3").arg(valueTruncated, 0, 'f', 1).arg(totalTruncated, 0, 'f', 1).arg(unit));
    ui->resourceProgress->setMaximum(total);
    ui->resourceProgress->setValue(value);
}

void SaveRequest::updateStatus()
{
    ui->okButton->setEnabled(loaded && (commitKey == -1) && (ui->collections->currentIndex() >= 0));
    ui->collections->setEnabled(commitKey == -1);
}

QString SaveRequest::getCurrentCollectionPath() const
{
    return collections.keys()[ui->collections->currentIndex()];
}
