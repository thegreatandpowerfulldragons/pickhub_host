#include "application.h"
#include <QMenu>
#include "server.h"
#include <QJsonObject>
#include <QJsonDocument>
#include "settings.h"
#include "settingswindow.h"
#include <QDir>
#include "collection.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QtConcurrent/QtConcurrent>
#include "browser.h"
#include <QFontDatabase>
#include <QMetaEnum>
#include "dss.h"
#include <QtMath>

QColor colorLerp(const QColor& a, const QColor& b, double alpha)
{
    return QColor(
        a.red()   + (b.red()   - a.red())   * alpha,
        a.green() + (b.green() - a.green()) * alpha,
        a.blue()  + (b.blue()  - a.blue())  * alpha,
        a.alpha() + (b.alpha() - a.alpha()) * alpha
    );
}

QMap<QString, QString> systemColors = {
    { "ubuntu",  "#E95420" },
    { "debian",  "#D70651" },
    { "windows", "#0077D5" }
};

Application* Application::instance = nullptr;

Application::Application(int& argc, char**& argv)
    : QApplication(argc, argv)
    , trayIcon(nullptr)
    , trayStop(nullptr)
    , trayRestart(nullptr)
    , settings(nullptr)
    , server(nullptr)
    , settingsWindow(nullptr)
    , failState(StartupFailState::None)
{
    // Ensure that app instance is unique
    lock = new QLockFile(applicationDirPath() + "/.lock");

    if (!lock->tryLock(1000))
    {
        qDebug() << "Instance of application already started";
        failState = StartupFailState::AlreadyStarted;
        return;
    }

    instance = this;

    // Setup icons
    icon = QIcon(QPixmap(":/icons/icon.png"));
    iconOnline = QIcon(QPixmap(":/icons/icon_online.png"));
    iconOffline = QIcon(QPixmap(":/icons/icon_offline.png"));

    // Setup fonts
    fontAwesomeFont = QFontDatabase::applicationFontFamilies(QFontDatabase::addApplicationFont(":/fonts/fontAwesome.otf")).at(0);

    // ---
    settings = new Settings(getSettingsPath(), this);

    // Setup Locale
    translator = new QTranslator(this);
    QCoreApplication::installTranslator(translator);

    if (!translator->load(settings->getLanguage(), getLocalPath("locales")))
    {
        if (settings->getLanguage() != "en")
        {
            if (!translator->load("en", getLocalPath("locales")))
            {
                qDebug() << QString("Unable to load locale %1, or, at least, en").arg(settings->getLanguage()).toStdString().c_str();
                failState = StartupFailState::UnableToLoadLocale;
                return;
            }
            else
            {
                qDebug() << QString("Unable to load locale %1, locale en loaded").arg(settings->getLanguage()).toStdString().c_str();
            }
        }
        else
        {
            qDebug() << "Unable to load locale en";
            failState = StartupFailState::UnableToLoadLocale;
            return;
        }
    }
    else
    {
        qDebug() << QString("Locale %1 loaded").arg(settings->getLanguage()).toStdString().c_str();
    }

    // Load style
    QString stylePath = getLocalPath("style.dss");
    QString styleDebugPath = getLocalPath("style_debug.qss");

    dss = new DSS(this);
    QString os = getOsName();
    QColor appColor = QColor(systemColors.contains(os) ? systemColors[os] : "#A5BE00");
    dss->bindParam("appColor", appColor.name());
    dss->bindParam("appColorDisabled", colorLerp(appColor, QColor(Qt::GlobalColor::gray), 0.5).name());

    if (QFile(stylePath).exists())
    {
        qDebug() << "External style file detected, processed version for debugging will be writen to" << styleDebugPath;

        dss->loadFromFile(stylePath);
        QFileSystemWatcher* watcher = new QFileSystemWatcher({ stylePath }, this);

        QFile styleDebug(styleDebugPath);
        if (styleDebug.open(QFile::OpenModeFlag::WriteOnly))
        {
            styleDebug.write(dss->toString().toUtf8());
            styleDebug.close();
        }

        connect(watcher, &QFileSystemWatcher::fileChanged, [=](const QString& path){
            dss->loadFromFile(path);

            QFile styleDebug(styleDebugPath);
            if (styleDebug.open(QFile::OpenModeFlag::WriteOnly))
            {
                styleDebug.write(dss->toString().toUtf8());
                styleDebug.close();
            }
        });
    }
    else
    {
        dss->loadFromFile(":/style.dss");
    }

    // Setup application settings
    setQuitOnLastWindowClosed(false);

    // Setup database
    QSqlDatabase::addDatabase("QSQLITE", "collection");

    // Prepare directories
    QDir().rmdir(getTempDir());
    QDir().mkdir(getTempDir());

    // Load collections
    QFile collectionsPathsFile(getCollectionsPath());
    if (collectionsPathsFile.open(QFile::OpenModeFlag::ReadOnly))
    {
        while (collectionsPathsFile.bytesAvailable())
        {
            QString path = collectionsPathsFile.readLine().trimmed();

            {
                QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", path);
                db.setDatabaseName(Collection::Paths(path).getDatabasePath());

                Collection collection(path);
                if (collection.isValid() && collection.fix())
                {
                    collectionsPaths.push_back(path);
                }
                else
                {
                    qDebug() << "Collection is not valid and removed:" << path;
                }
            }

            QSqlDatabase::removeDatabase(path);
        }
        collectionsPathsFile.close();
    }
    if (collectionsPathsFile.open(QFile::OpenModeFlag::WriteOnly))
    {
        for (const auto& path : collectionsPaths)
        {
            collectionsPathsFile.write((path + "\n").toUtf8());
        }
    }

    // Setup tray
    QMenu* trayMenu = new QMenu();

    trayCollections = new QMenu(tr("Collections"));
    trayMenu->addMenu(trayCollections);

    trayCollectionsSeparator = trayCollections->addSeparator();
    trayCollectionsSeparator->setVisible(!collectionsPaths.empty());

    trayCollectionsAdd = new QAction(tr("Add..."), trayCollections);
    trayCollections->addAction(trayCollectionsAdd);

    for (const auto& path : collectionsPaths)
    {
        attachCollection(path);
    }

    QAction* traySettings = new QAction(tr("Settings..."), trayMenu);
    trayMenu->addAction(traySettings);

    trayStop = new QAction(tr("Stop server"), trayMenu);
    trayStop->setVisible(false);
    trayMenu->addAction(trayStop);

    trayRestart = new QAction(tr("Restart server"), trayMenu);
    trayRestart->setVisible(false);
    trayMenu->addAction(trayRestart);

    QAction* trayQuit = new QAction(tr("Quit"), trayMenu);
    trayMenu->addAction(trayQuit);

    trayIcon = new QSystemTrayIcon();
    trayIcon->setIcon(iconOffline);
    trayIcon->setToolTip("PickHub host");
    trayIcon->setContextMenu(trayMenu);
    trayIcon->show();

    // Run server
    server = new Server(this);

    // Setup connections
    connect(settings, &Settings::portChanged, [=](){
        if (server->isStarted()) server->restart();
    });
    connect(traySettings, SIGNAL(triggered()), this, SLOT(showSettings()));
    connect(trayStop, SIGNAL(triggered()), server, SLOT(stop()));
    connect(trayRestart, SIGNAL(triggered()), server, SLOT(restart()));
    connect(trayQuit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(trayCollectionsAdd, SIGNAL(triggered()), this, SLOT(showCollectionAdd()));
}

Application::~Application()
{
    lock->unlock();
}

const QIcon& Application::getIcon()
{
    return instance->icon;
}

const QIcon& Application::getIconOnline()
{
    return instance->iconOnline;
}

const QIcon& Application::getIconOffline()
{
    return instance->iconOffline;
}

const QFont& Application::getFontAwesomeFont()
{
    return instance->fontAwesomeFont;
}

void Application::setupTrayIconOnline()
{
    instance->trayIcon->setIcon(instance->iconOnline);
}

void Application::setupTrayIconOffline()
{
    instance->trayIcon->setIcon(instance->iconOffline);
}

Settings* Application::getSettings()
{
    return instance->settings;
}

Server* Application::getServer()
{
    return instance->server;
}

void Application::setupTrayRestart()
{
    instance->trayRestart->setVisible(true);
    instance->trayStop->setVisible(false);
}

void Application::setupTrayStop()
{
    instance->trayRestart->setVisible(false);
    instance->trayStop->setVisible(true);
}

QString Application::getLocalPath(const QString& filename)
{
    return applicationDirPath() + "/" + filename;
}

QString Application::getTempDir()
{
    return getLocalPath(".temp");
}

QString Application::getCollectionsPath()
{
    return getLocalPath("collections");
}

QString Application::getSettingsPath()
{
    return getLocalPath("settings.json");
}

const QStringList& Application::getCollectionsPaths()
{
    return instance->collectionsPaths;
}

void Application::addCollectionPath(const QString& path)
{
    if (instance->collectionsPaths.contains(path)) return;

    instance->collectionsPaths.push_back(path);

    instance->attachCollection(path);

    Collection(path).fix();

    instance->server->syncCollections();

    QFile collectionsPathsFile(getCollectionsPath());
    if (collectionsPathsFile.open(QFile::OpenModeFlag::Append))
    {
        collectionsPathsFile.write((path + "\n").toUtf8());
        collectionsPathsFile.close();
    }
}

void Application::removeCollectionPath(const QString& path)
{
    int index = instance->collectionsPaths.indexOf(path);

    if (index >= 0)
    {
        instance->detachCollection(index);

        instance->collectionsPaths.removeAt(index);

        instance->server->syncCollections();

        QFile collectionsPathsFile(getCollectionsPath());
        if (collectionsPathsFile.open(QFile::OpenModeFlag::WriteOnly))
        {
            for (const auto& path : instance->collectionsPaths)
            {
                collectionsPathsFile.write((path + "\n").toUtf8());
            }
            collectionsPathsFile.close();
        }
    }
}

DSS* Application::getDss()
{
    return instance->dss;
}

Application::StartupFailState Application::getFailState()
{
    return instance->failState;
}

void Application::showSettings()
{
    settingsWindow = new SettingsWindow(getSettings());
    settingsWindow->show();
    settingsWindow->raise();
}

void Application::showCollection(const QString& path)
{
    if (Collection(path).isValid())
    {
        if (browsers.contains(path))
        {
            browsers[path]->raise();
        }
        else
        {
            Browser* browser = new Browser(Collection::get(path));
            browser->show();
            browser->raise();

            browsers[path] = browser;

            connect(browser, &Browser::destroyed, [=](){
                browsers.remove(path);
            });
        }
    }
    else
    {
        removeCollectionPath(path);
    }
}

void Application::showCollectionAdd()
{
    QFileDialog* dialog = new QFileDialog();
    dialog->setDirectory(QStandardPaths::locate(QStandardPaths::StandardLocation::HomeLocation, "", QStandardPaths::LocateOption::LocateDirectory));
    dialog->setWindowIcon(getIcon());
    dialog->setFileMode(QFileDialog::FileMode::Directory);
    dialog->show();

    connect(dialog, &QFileDialog::finished, [=](int state){
        if (state)
        {
            addCollectionPath(dialog->directory().path());
        }
        delete dialog;
    });
}

void Application::updateLanguage()
{

}

void Application::attachCollection(const QString& path)
{   
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE", path);
    database.setDatabaseName(Collection::Paths(path).getDatabasePath());

    QAction* action = new QAction(QFileInfo(path).baseName());
    trayCollections->insertAction(trayCollectionsSeparator, action);

    Collection* collection = Collection::get(path);
    collection->fix();

    collection->watch();

    trayCollectionsSeparator->setVisible(trayCollections->actions().count() > 2);
    trayCollections->update();

    connect(action, &QAction::triggered, [=](){
        showCollection(path);
    });
    connect(collection, &Collection::deletedByUser, [=](){
        removeCollectionPath(path);
    });

    qDebug() << "Attached collection:" << path;
}

void Application::detachCollection(int index)
{
    QSqlDatabase::removeDatabase(collectionsPaths[index]);

    if (index >= 0 && index < trayCollections->actions().count() - 2)
    {
        delete trayCollections->actions()[index];
    }

    qDebug() << "Detached collection:" << collectionsPaths[index];

    collectionsPaths.removeAt(index);

    trayCollections->update();
}

QString Application::getOsName() const
{
#if defined(Q_OS_WINDOWS)
    return "windows";
#elif defined (Q_OS_UNIX)
    QFile scriptFile(":/distro.sh");
    scriptFile.open(QFile::OpenModeFlag::ReadOnly);
    QString script = scriptFile.readAll();
    scriptFile.close();

    QProcess shell;
    shell.setProgram("/bin/bash");
    shell.setArguments({ "-c", script });
    shell.start();
    shell.waitForFinished();
    QString os = shell.readAll().trimmed().toLower();
    shell.close();

    if (shell.exitCode() == 0)
    {
        return os;
    }
    else
    {
        return "";
    }
#endif
}
