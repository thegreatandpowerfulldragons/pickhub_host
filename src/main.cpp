#include "server.h"
#include <application.h>
#include "collection.h"
#include <QPixmapCache>

int main(int argc, char* argv[])
{
    qRegisterMetaType<Collection::Item>("Collection::Item");
    qRegisterMetaType<Collection::ItemShort>("Collection::ItemShort");

    Application a(argc, argv);

    if (a.getFailState() == Application::StartupFailState::None)
    {
        return a.exec();
    }
    else
    {
        return (int)a.getFailState();
    }
}
