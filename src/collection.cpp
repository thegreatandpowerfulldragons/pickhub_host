#include "collection.h"
#include <QFile>
#include <QtCore>
#include <QSqlError>
#include <QSqlQuery>
#include <QtConcurrent/QtConcurrent>
#include <QPixmap>
#include <QRgb>
#include <QMessageBox>
#include "boolinq/include/boolinq/boolinq.h"
#include "hashprint.h"
#include <QMovie>
#include <QMediaPlayer>

#define checkCommitProcessing()\
if (commitSchedule[commitKey] != CommitState::Processing)\
{\
    QFile(filePath).remove();\
    QFile(filePath + "_thumbnail").remove();\
    database.close();\
    restore();\
    lockFile.unlock();\
    emit commitFinished(commitKey, false);\
    return;\
}

#define strToType(str) static_cast<ItemType>(QMetaEnum::fromType<ItemType>().keyToValue(str))

QMap<QString, Collection*> Collection::collections = QMap<QString, Collection*>();

Collection::Paths::Paths(const QString& path)
    : path(path)
{

}

QString Collection::Paths::getPath(const QString& subpath) const
{
    if (subpath.isEmpty())
    {
        return path;
    }
    else
    {
        return path + "/" + subpath;
    }
}

QString Collection::Paths::getMetaPath() const
{
    return path + "/.pickhub";
}

QString Collection::Paths::getDatabasePath() const
{
    return getMetaPath() + "/meta";
}

QString Collection::Paths::getDatabaseBackupPath() const
{
    return getDatabasePath() + ".backup";
}

QString Collection::Paths::getCommitLockPath() const
{
    return getMetaPath() + "/commit_lock";
}

Collection* Collection::get(const QString& path)
{
    if (collections.contains(path))
    {
        return collections[path];
    }
    else
    {
        Collection* collection = new Collection(path);
        collections[path] = collection;
        return collection;
    }
}

void Collection::close(const QString &path)
{
    if (collections.contains(path))
    {
        delete collections[path];
        collections.remove(path);
    }
}

Collection::Collection(const QString& path)
    : QObject(nullptr)
    , paths(path)
    , verified(false)
    , watcher(nullptr)
{
    database = QSqlDatabase::database(path, false);
}

Collection::~Collection()
{
    if (watcher != nullptr)
    {
        unwatch();
    }
}

void Collection::watch()
{
    QStringList watchList;

    auto tryToAddPath = [&](const QString& path){
        if (watcher == nullptr || !watchPaths.contains(path))
        {
            watchList.push_back(path);
        }
    };

    tryToAddPath(paths.getMetaPath());

    QVector<ItemShort> itemList;
    if (getAllItems(itemList))
    {
        for (const auto& item : itemList)
        {
            tryToAddPath(paths.getPath(QString::number(item.id)));
        }
    }

    if (watcher == nullptr)
    {
        watcher = new QFileSystemWatcher(watchList, this);
        connect(watcher, &QFileSystemWatcher::directoryChanged, [=](const QString& path){
            if (path == paths.getMetaPath() && !isValid())
            {
                deleteLater();
                emit deletedByUser();
            }
        });
        connect(watcher, &QFileSystemWatcher::fileChanged, [=](const QString& path){
            QString filename = QFileInfo(path).baseName();
            bool isok;
            uint id = filename.toUInt(&isok);
            if (isok)
            {
                remove(id);
            }
        });
    }
    else
    {
        watcher->addPaths(watchList);
    }
}

void Collection::unwatch()
{
    delete watcher;
    watcher = nullptr;
    watchPaths.clear();
}

bool Collection::isValid()
{
    if  (!QDir(paths.getMetaPath()).exists()) return false;

    if  (!QFile(paths.getDatabasePath()).exists()) return false;

    return true;
}

bool Collection::isVerified() const
{
    return verified;
}

QString Collection::getName() const
{
    return QFileInfo(paths.getPath()).baseName();
}

uint Collection::getVersion()
{
    if (database.open())
    {
        QSqlQuery query(database);
        query.clear();
        query.prepare("PRAGMA user_version;");
        query.exec();
        query.next();

        database.close();

        return query.value("user_version").toUInt();
    }
    else
    {
        return 0;
    }
}

int Collection::commit(const QString& tempFilePath, const QUrl& source, const QUrl& page, const QStringList& tags, ItemType type)
{
    if (!verified)
    {
        return -1;
    }

    int commitKey = commitSchedule.size();
    commitSchedule.push_back(CommitState::Pending);

    QtConcurrent::run([=](){
        QSqlQuery query(database);
        QMap<QString, int> tagId;
        QFile temp(tempFilePath);
        QFileInfo tempInfo(tempFilePath);

        bool captureThumbnail = type == ItemType::Animation || type == ItemType::Video;

        // 0 Pending
        // 1 Checking hash
        // n Processing tag
        // 1 Adding media
        // n Attaching tag
        // 1 Copying item
        // 1 Generating thumbnail
        // 1 Saving thumbnail
        // 1 Done
        uint total = 1 + tags.size() + 1 + tags.size() + 1;
        if (captureThumbnail) total += 2;

        emit commitProgress(commitKey, 0, total, tr("Pending..."));

        Item item;
        item.format = tempInfo.suffix();
        if (type == ItemType::Image)
        {
            item.hash = new HashPrint(QPixmap(tempFilePath));
        }
        item.source = source.toString();
        item.page = page.toString();
        item.time = QDateTime::currentDateTime().toUTC();
        item.tags = tags;
        item.type = type;

        QLockFile lockFile(paths.getCommitLockPath());

        while (!lockFile.lock())
        {
            if (commitSchedule[commitKey] != CommitState::Pending)
            {
                emit commitFinished(commitKey, false);
                return;
            }
        }

        commitSchedule[commitKey] = CommitState::Processing;

        QString filePath = paths.getPath(QString::number(item.id));

        if (!database.open())
        {
            qDebug() << databaseInaccessibleString();

            commitSchedule[commitKey] = CommitState::Errored;

            checkCommitProcessing();
        }

        backup();

        checkCommitProcessing();

        emit commitProgress(commitKey, 1, total, tr("Checking hash..."));

        if (item.hash != nullptr)
        {
            query.clear();
            query.prepare("SELECT COUNT(*) AS count FROM media WHERE hash = :hash");
            query.bindValue(":hash", item.hash->toBytes(), QSql::ParamTypeFlag::In | QSql::ParamTypeFlag::Binary);
            query.exec();
            query.next();
            bool duplicate = query.value("count").toInt() > 0;

            if (duplicate)
            {
                QMessageBox(QMessageBox::Icon::Warning, tr("Error"), tr("Same hash was found")).exec();

                commitSchedule[commitKey] = CommitState::Errored;

                checkCommitProcessing();
            }
        }

        for (int i = 0; i < tags.size(); i++)
        {
            const QString tag = tags[i];

            checkCommitProcessing();

            emit commitProgress(commitKey, 1 + i, total, tr("Processing tag") + " \"" + tag + "\"...");

            query.clear();
            query.prepare("SELECT * "
                          "FROM tags "
                          "WHERE name = :name;"
                          );
            query.bindValue(":name", tag);
            query.exec();

            if (query.next())
            {
                tagId[tag] = query.value("id").toInt();
            }
            else
            {
                checkCommitProcessing();

                query.clear();
                query.prepare("INSERT "
                              "INTO tags (name) "
                              "VALUES(:name);"
                              );
                query.bindValue(":name", tag);
                query.exec();

                tagId[tag] = query.lastInsertId().toInt();
            }
        }

        checkCommitProcessing();

        emit commitProgress(commitKey, 1 + tags.size(), total, tr("Adding media..."));

        query.clear();
        query.prepare("INSERT "
                      "INTO media (format, hash, source, page, time, type) "
                      "VALUES (:format, :hash, :source, :page, :time, :type);"
                      );
        query.bindValue(":format", item.format);
        if (item.hash != nullptr)
        {
            query.bindValue(":hash", item.hash->toBytes(), QSql::ParamTypeFlag::In | QSql::ParamTypeFlag::Binary);
        }
        else
        {
            query.bindValue(":hash", QVariant(QVariant::ByteArray), QSql::ParamTypeFlag::In | QSql::ParamTypeFlag::Binary);
        }
        query.bindValue(":source", item.source);
        query.bindValue(":page", item.page);
        query.bindValue(":time", item.time);
        query.bindValue(":type", QMetaEnum::fromType<ItemType>().valueToKey((int)type));
        query.exec();

        item.id = query.lastInsertId().toUInt();

        for (int i = 0; i < tagId.size(); i++)
        {
            const QString tag = tagId.keys()[i];

            checkCommitProcessing();

            emit commitProgress(commitKey, 1 + tags.size() + 1 + i, total, tr("Attaching tag") + " \"" + tag + "\"...");

            query.clear();
            query.prepare("INSERT "
                          "INTO media_tag (media, tag) "
                          "VALUES (:media, :tag);"
                          );
            query.bindValue(":media", item.id);
            query.bindValue(":tag", tagId[tag]);
            query.exec();
        }

        emit commitProgress(commitKey, 1 + tags.size() + 1 + tags.size() + 0, total, tr("Copying media"));

        QFile(tempFilePath).copy(filePath);

        emit commitProgress(commitKey, 1 + tags.size() + 1 + tags.size() + 1, total, tr("Generating thumbnail"));

        QPixmap thumbnail;
        if (captureThumbnail)
        {
            QEventLoop thumbnailLoop;
            thumbnailLoop.connect(this, &Collection::thumbnailGenerated, [&](const QPixmap& result){
                thumbnail = result;
                thumbnailLoop.quit();
            });

            QMetaObject::invokeMethod(qApp, [&](){
                emit thumbnailGenerated(generateThumbnail(tempFilePath, item.type));
            });

            thumbnailLoop.exec();

            if (false)
            {
                qDebug() << "Thumbnail for " << tempFilePath << " could not be created";

                commitSchedule[commitKey] = CommitState::Errored;

                checkCommitProcessing();
            }

            if (!thumbnail.isNull())
            {
                emit commitProgress(commitKey, 1 + tags.size() + 1 + tags.size() + 2, total, tr("Saving thumbnail"));
                thumbnail.save(filePath + "_thumbnail");
            }
            else
            {
                qDebug() << "Error capturing thumbnail for " << tempFilePath;

                commitSchedule[commitKey] = CommitState::Errored;

                checkCommitProcessing();
            }
        }

        emit commitProgress(commitKey, total, total, tr("Done"));

        if (watcher != nullptr)
        {
            watch(filePath);
        }

        database.close();

        lockFile.unlock();

        commitSchedule[commitKey] = CommitState::Finished;

        emit itemAdded(item);

        emit commitFinished(commitKey, true);
    });

    return commitKey;
}

void Collection::cancelCommit(int commitKey)
{
    if (commitKey >= 0 && commitKey < commitSchedule.size())
    {
        commitSchedule[commitKey] = CommitState::Canceled;
    }
}

bool Collection::remove(uint id)
{
    if (!verified)
    {
        return false;
    }

    if (!database.open())
    {
        qDebug() << databaseInaccessibleString();
        return false;
    }

    QSqlQuery query(database);

    query.clear();
    query.prepare("DELETE "
                  "FROM media WHERE id = :id;"
                  );
    query.bindValue(":id", id);
    query.exec();

    query.clear();
    query.prepare("DELETE "
                  "FROM media_tag WHERE media = :media;"
                  );
    query.bindValue(":media", id);
    query.exec();

    QString filePath = paths.getPath(QString::number(id));

    QFile(filePath).remove();
    QFile(filePath + "_thumbnail").remove();

    if (watcher != nullptr)
    {
        unwatch(filePath);
    }

    emit itemRemoved(id);

    database.close();

    return true;
}

bool Collection::getItem(uint id, Item& result)
{
    if (!verified)
    {
        return false;
    }

    if (!database.open())
    {
        qDebug() << databaseInaccessibleString();
        return false;
    }

    result = Item();

    QSqlQuery query(database);

    query.clear();
    query.prepare("SELECT * "
                  "FROM media WHERE id = :id;"
                  );
    query.bindValue(":id", id);
    query.exec();

    if (query.next())
    {
        QByteArray hashBytes;

        result.id = query.value("id").toUInt();
        result.format = query.value("format").toString();
        hashBytes = query.value("hash").toByteArray();
        result.source = query.value("source").toString();
        result.page = query.value("page").toString();
        result.time = query.value("time").toDateTime().toLocalTime();
        result.type = strToType(query.value("type").toString().toUtf8().data());

        if (hashBytes.length() == 0)
        {
            result.hash = nullptr;
        }
        else if (hashBytes.length() == 32)
        {
            result.hash = new HashPrint(hashBytes);
        }
    }
    else
    {
        return false;
    }

    query.clear();
    query.prepare("SELECT tags.name "
                  "FROM media_tag "
                  "INNER JOIN tags ON tags.id = media_tag.tag "
                  "WHERE media_tag.media = :media "
                  "ORDER BY tags.name"
                  );
    query.bindValue(":media", result.id);
    query.exec();

    while(query.next())
    {
        result.tags.push_back(query.value("name").toString());
    }

    database.close();

    return true;
}

bool Collection::searchItems(QVector<ItemShort>& out, const QString& searchPattern)
{
    if (!verified)
    {
        return false;
    }

    if (!database.open())
    {
        qDebug() << databaseInaccessibleString();
        return false;
    }

    QString trimmerSearch = searchPattern.trimmed();

    QStringList queryParts = {
        "SELECT id, type "
        "FROM media"
    };

    QStringList addRules;
    QStringList removeRules;

    if (!trimmerSearch.isEmpty())
    {
        queryParts += "WHERE";

        QStringList patternParts;

        QStringList rules = trimmerSearch.split(' ', QString::SplitBehavior::SkipEmptyParts);

        for (const auto& rule : rules)
        {
            if (rule.startsWith('-'))
            {
                removeRules += rule.mid(1);
            }
            else
            {
                addRules += rule;
            }
        }

        if (addRules.length() > 0)
        {
            patternParts += "id IN ( "
                            "  SELECT media "
                            "  FROM media_tag "
                            "  WHERE tag IN ( "
                            "    SELECT id "
                            "    FROM tags "
                            "    WHERE name IN (" + QStringList::fromStdList(boolinq::from(addRules.toStdList()).select_i([](const QString&, int index){ return ":add_" + QString::number(index); }).toStdList()).join(", ") + ") "
                            "  ) "
                            "  GROUP BY media "
                            "  HAVING COUNT(DISTINCT tag) = :add_tag_count "
                            ")";
        }

        if (removeRules.length() > 0)
        {
            patternParts += "id NOT IN ( "
                            "  SELECT media "
                            "  FROM media_tag "
                            "  WHERE tag IN ( "
                            "    SELECT id "
                            "    FROM tags "
                            "    WHERE name IN (" + QStringList::fromStdList(boolinq::from(removeRules.toStdList()).select_i([](const QString&, int index){ return ":remove_" + QString::number(index); }).toStdList()).join(", ") + ") "
                            "  ) "
                            "  GROUP BY media "
                            "  HAVING COUNT(DISTINCT tag) = :remove_tag_count "
                            ")";
        }

        queryParts += patternParts.join(" AND ");
    }

    QSqlQuery query(database);
    query.clear();
    query.prepare(queryParts.join(' '));

    if (!addRules.isEmpty())
    {
        for (int i = 0; i < addRules.length(); i++)
        {
            query.bindValue(":add_" + QString::number(i), addRules[i]);
        }
        query.bindValue(":add_tag_count", addRules.length());
    }

    if (!removeRules.isEmpty())
    {
        for (int i = 0; i < removeRules.length(); i++)
        {
            query.bindValue(":remove_" + QString::number(i), removeRules[i]);
        }
        query.bindValue(":remove_tag_count", removeRules.length());
    }

    query.exec();

    out.clear();
    while (query.next())
    {
        ItemShort item;
        item.id = query.value("id").toUInt();
        item.type = strToType(query.value("type").toString().toUtf8().data());

        out += item;
    }

    database.close();

    return true;
}

bool Collection::getAllItems(QVector<Collection::ItemShort>& out)
{
    if (!verified)
    {
        return false;
    }

    if (!database.open())
    {
        qDebug() << databaseInaccessibleString();
        return false;
    }

    QSqlQuery query(database);

    query.clear();
    query.prepare("SELECT id, type FROM media");
    query.exec();

    out.clear();
    while (query.next())
    {
        ItemShort item;
        item.id = query.value("id").toUInt();
        item.type = strToType(query.value("type").toString().toUtf8().data());

        out += item;
    }

    database.close();

    return true;
}

bool Collection::fix()
{
    QDir().mkdir(paths.getMetaPath());

    if (QFile(paths.getCommitLockPath()).exists())
    {
        restore();
        QFile(paths.getCommitLockPath()).remove();
    }

    uint sqlVersionPrev = getVersion();

    // Updating database scheme from latest versions
    if (sqlVersionPrev < sqlVersion)
    {
        if (database.open())
        {
            while (sqlVersionPrev < sqlVersion)
            {
                QFile updateScript(QString(":/db/migration_%1.sql").arg(sqlVersionPrev));

                if (!updateScript.exists() && sqlVersionPrev < sqlVersion)
                {
                    return false;
                }

                if (updateScript.open(QFile::OpenModeFlag::ReadOnly))
                {
                    backup();

                    bool failed = false;
                    QStringList statements = QString(updateScript.readAll()).trimmed().split(';', QString::SplitBehavior::SkipEmptyParts);
                    for (const auto& statement : statements)
                    {
                        QSqlQuery query(database);
                        if (!query.exec(statement.trimmed()))
                        {
                            failed = true;
                            break;
                        }
                    }

                    if (failed)
                    {
                        restore();
                        qDebug() << "Unable to update database from version" << sqlVersionPrev;
                        database.close();
                        break;
                    }
                    else
                    {
                        sqlVersionPrev = getVersion();
                    }
                }
                else
                {
                    return false;
                }
            }

            if (sqlVersionPrev > sqlVersion)
            {

            }

            if (database.isOpen())
            {
                database.close();
            }
        }
        else
        {
               qDebug() << databaseInaccessibleString();
               return false;
        }
    }

    verified = true;

    return true;
}

QPixmap Collection::generateThumbnail(const QString& filepath, Collection::ItemType type)
{
    if (type == Collection::ItemType::Animation)
    {
        QMovie movie(filepath);
        movie.setCacheMode(QMovie::CacheMode::CacheAll);
        movie.jumpToFrame(movie.frameCount() / 2);
        return movie.currentPixmap();
    }
    else if (type == Collection::ItemType::Video)
    {
        VideoCapture capture;

        QMediaPlayer player;
        player.setMuted(true);
        player.setVideoOutput(&capture);

        QEventLoop loop;
        loop.connect(&player, &QMediaPlayer::mediaStatusChanged, [&](QMediaPlayer::MediaStatus){
            loop.quit();
        });

        player.setMedia(QUrl::fromLocalFile(filepath));

        loop.exec();

        if (player.mediaStatus() == QMediaPlayer::MediaStatus::InvalidMedia)
        {
            return QPixmap();
        }

        player.play();
        player.setPosition(player.duration() / 2);
        player.pause();

        return capture.getPixmap();
    }
}

const char* Collection::databaseInaccessibleString() const
{
    return QString("Database file \"%1\" is inaccessible: %2").arg(paths.getDatabasePath()).arg(database.lastError().text()).toStdString().c_str();
}

void Collection::watch(const QString& file)
{
    watcher->addPath(file);
    watchPaths.push_back(file);
}

void Collection::unwatch(const QString& file)
{
    watcher->removePath(file);
    watchPaths.removeAll(file);
}

void Collection::backup()
{
    QFile(paths.getDatabaseBackupPath()).remove();
    QFile(paths.getDatabasePath()).copy(paths.getDatabaseBackupPath());
}

void Collection::restore()
{
    QFile(paths.getDatabasePath()).remove();
    QFile(paths.getDatabaseBackupPath()).copy(paths.getDatabasePath());
}

QList<QVideoFrame::PixelFormat> Collection::VideoCapture::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
{
    if (handleType == QAbstractVideoBuffer::NoHandle)
    {
        return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_RGB24;
    }
    else
    {
        return QList<QVideoFrame::PixelFormat>();
    }
}

bool Collection::VideoCapture::present(const QVideoFrame& frame)
{
    if (supportedPixelFormats(frame.handleType()).contains((frame.pixelFormat())))
    {
        setError(IncorrectFormatError);
        return false;
    }
    else
    {
        QVideoFrame frametodraw(frame);

        if(!frametodraw.map(QAbstractVideoBuffer::ReadOnly))
        {
            setError(ResourceError);
            return false;
        }

        pixmap = QPixmap::fromImage(QImage(frametodraw.bits(), frametodraw.width(), frametodraw.height(), frametodraw.bytesPerLine(), QImage::Format::Format_RGB444));

        return true;
    }
}

const QPixmap& Collection::VideoCapture::getPixmap()
{
    return pixmap;
}
