#include "dss.h"
#include <functional>

void replaceAll(QString& target, const QRegularExpression& rx, std::function<QString(QRegularExpressionMatch)> pred)
{
    if (!rx.isValid()) return;

    forever {
        auto match = rx.match(target);
        if (match.hasMatch())
        {
            QString occurrence = match.captured();
            target.replace(target.indexOf(occurrence), occurrence.length(), pred(match));
        }
        else
        {
            break;
        }
    }
}

DSS::DSS(QObject* parent)
    : QObject(parent)
{

}

DSS::DSS(const QString& path, QObject* parent)
    : DSS(parent)
{
    QFile file(path);

    if (file.open(QFile::OpenModeFlag::ReadOnly))
    {
        load(file.readAll());
        file.close();
    }
}

void DSS::load(const QString& dss)
{
    QStringList lines = dss.split('\n');
    source = "";

    for (auto& line : lines)
    {
        line = line.trimmed();
        if (line.startsWith('@'))
        {
            if (line.contains('='))
            {
                QString name = line.mid(1, line.indexOf('=') - 1).trimmed();
                QString value = line.mid(line.indexOf('=') + 1).trimmed();
                params[name] = value;
            }

            source += '\n';
        }
        else
        {
            source += line + '\n';
        }
    }

    process();
    emit updated();
}

void DSS::loadFromFile(const QString& path)
{
    QFile dssFile(path);
    if (dssFile.open(QFile::OpenModeFlag::ReadOnly))
    {
        load(dssFile.readAll());
        dssFile.close();
    }
}

void DSS::bindParam(const QString& name, const QString& value)
{
    if (params.contains(name) && params[name] == value)
    {
        return;
    }

    params[name] = value;
    process();
    emit updated();
}

void DSS::bindParams(const QMap<QString, QString>& paramsMap)
{
    bool shouldProcess = false;
    for (const auto& name : params.keys())
    {
        const QString& value = paramsMap[name];
        if (params.contains(name) && params[name] == value)
        {
            continue;
        }

        params[name] = value;
        shouldProcess = true;
    }

    if (shouldProcess)
    {
        process();
        emit updated();
    }
}

QString DSS::getParam(const QString& name, const QString& defaultValue)
{
    if (params.contains(name))
    {
        return params[name];
    }
    else
    {
        return defaultValue;
    }
}

const QString& DSS::toString() const
{
    return cache;
}

DSS::operator QString&()
{
    return cache;
}

void DSS::process()
{
    cache = source;

    replaceAll(cache, paramRX, [=](QRegularExpressionMatch match){
        return params[match.captured("name")];
    });

    replaceAll(cache, eachRX, [=](QRegularExpressionMatch match){
        QStringList parts = match.captured("inputs").split(',');
        QString modifier = match.captured("modifier");

        for (int i = 0; i < parts.length(); i++)
        {
            parts[i] = parts[i].trimmed() + modifier;
        }

        return parts.join(", ");
    });
}
