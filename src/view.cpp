#include "view.h"
#include "ui_view.h"
#include "previewimage.h"
#include "previewanimation.h"
#include <QResizeEvent>
#include "application.h"
#include "dss.h"

View::View(Collection* collection, uint id)
    : QWidget(nullptr)
    , ui(new Ui::View)
    , preview(nullptr)
    , collection(collection)
    , id(id)
{
    ui->setupUi(this);

    Collection::Item item;
    if (!collection->getItem(id, item))
    {
        deleteLater();
        return;
    }

    setStyleSheet(Application::getDss()->toString());

    setWindowTitle(tr("%1 item from %2").arg(QMetaEnum::fromType<Collection::ItemType>().valueToKey((int)item.type)).arg(collection->getName()));

    switch (item.type)
    {
    case Collection::ItemType::Image:
        preview = new PreviewImage(this);
        break;

    case Collection::ItemType::Animation:
        preview = new PreviewAnimation(this);
        break;

    case Collection::ItemType::Video:
        throw "Video view not implemented";
        break;

    case Collection::ItemType::Audio:
        throw "Audio view not implemented";
        break;
    }

    ui->display->layout()->addWidget(preview);

    connect(Application::getDss(), &DSS::updated, [=](){
        setStyleSheet(Application::getDss()->toString());
    });
}

View::~View()
{
    delete ui;
}

void View::showEvent(QShowEvent* event)
{
    preview->open(getFileName());

    QWidget::showEvent(event);
}

void View::closeEvent(QCloseEvent*)
{
    deleteLater();
}

QString View::getFileName() const
{
    return collection->paths.getPath() + "/" + QString::number(id);
}
