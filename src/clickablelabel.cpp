#include "clickablelabel.h"
#include <QMouseEvent>
#include <QHBoxLayout>
#include "application.h"

#define iconsize 15, 15

ClickableLabel::ClickableLabel(QWidget* parent)
    : ClickableLabel("", QPixmap(0, 0), parent)
{

}

ClickableLabel::ClickableLabel(const QString& text, const QPixmap& pixmap, QWidget* parent)
    : QWidget(parent)
    , wasOver(false)

{
    setLayout(new QHBoxLayout());
    layout()->setMargin(0);
    layout()->setSpacing(2);
    setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));

    textLabel = new QLabel(text);
    iconLabel = new QLabel();

    layout()->addWidget(textLabel);
    layout()->addWidget(iconLabel);

    textLabel->setCursor(Qt::PointingHandCursor);
    textLabel->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Preferred));

    QFont fa = Application::getFontAwesomeFont();
    fa.setPixelSize(8);
    iconLabel->setFont(fa);

    if (pixmap.isNull())
    {
        iconLabel->clear();
    }
    else
    {
        iconLabel->setPixmap(pixmap.scaled(iconsize, Qt::AspectRatioMode::KeepAspectRatio));
    }
    iconLabel->setAlignment(Qt::Alignment(Qt::AlignmentFlag::AlignLeft | Qt::AlignmentFlag::AlignTop));
    iconLabel->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum));
    iconLabel->setMaximumSize(iconsize);
    iconLabel->setMinimumSize(iconsize);
    iconLabel->setVisible(false);
}

ClickableLabel::~ClickableLabel()
{
    delete textLabel;
    delete iconLabel;
}

void ClickableLabel::setText(const QString& value)
{
    textLabel->setText(value);
    emit textChanged(getText());
}

QString ClickableLabel::getText() const
{
    return textLabel->text();
}

void ClickableLabel::setIcon(const QString& unicodeChar)
{
    iconLabel->setText(unicodeChar);
    emit iconChanged(getIcon());
}

QString ClickableLabel::getIcon() const
{
    return iconLabel->text();
}

void ClickableLabel::setFont(const QFont& value)
{
    textLabel->setFont(value);
}

QFont ClickableLabel::getFont() const
{
    return textLabel->font();
}

void ClickableLabel::enterEvent(QEvent*)
{
    iconLabel->setVisible(true);
    QFont f = textLabel->font();
    f.setUnderline(true);
    textLabel->setFont(f);
}

void ClickableLabel::leaveEvent(QEvent*)
{
    iconLabel->setVisible(false);
    QFont f = textLabel->font();
    f.setUnderline(false);
    textLabel->setFont(f);
}

void ClickableLabel::mousePressEvent(QMouseEvent*)
{
    emit clicked();
}
