#include "hashprint.h"
#include <QImage>
#include <QPixmap>

HashPrint::HashPrint()
{

}

HashPrint::HashPrint(const QPixmap& pixmap)
{
    QImage img = pixmap.toImage().scaled(16, 16, Qt::AspectRatioMode::IgnoreAspectRatio, Qt::TransformationMode::SmoothTransformation).convertToFormat(QImage::Format::Format_Grayscale8);

    int average = 0;

    for (uchar y = 0; y < 16; y ++)
    {
        for (uchar x = 0; x < 16; x++)
        {
            average += img.pixelColor(x, y).red();
        }
    }

    average /= 16 * 16;

    for (uchar y = 0; y < 16; y ++)
    {
        for (uchar x = 0; x < 16; x++)
        {
            data[x][y] = img.pixelColor(x, y).red() > average;
        }
    }
}

HashPrint::HashPrint(const QByteArray& bytes)
{
    if (bytes.length() == 32)
    {
        for (uchar x = 0; x < 16; x++)
        {
            for (uchar i = 0; i < 2; i++)
            {
                uchar b = bytes[x * 2 + i];

                data[x][i * 8 + 0] = b & 128;
                data[x][i * 8 + 1] = b &  64;
                data[x][i * 8 + 2] = b &  32;
                data[x][i * 8 + 3] = b &  16;
                data[x][i * 8 + 4] = b &   8;
                data[x][i * 8 + 5] = b &   4;
                data[x][i * 8 + 6] = b &   2;
                data[x][i * 8 + 7] = b &   1;
            }
        }
    }
    else
    {
        qDebug() << "wrong array length:" << bytes.length();
    }
}

QByteArray HashPrint::toBytes() const
{
    QByteArray bytes;

    for (uchar x = 0; x < 16; x++)
    {
        for (uchar i = 0; i < 2; i++)
        {
            uchar b = 0;

            if (data[x][i * 8 + 0]) b |= 128;
            if (data[x][i * 8 + 1]) b |=  64;
            if (data[x][i * 8 + 2]) b |=  32;
            if (data[x][i * 8 + 3]) b |=  16;
            if (data[x][i * 8 + 4]) b |=   8;
            if (data[x][i * 8 + 5]) b |=   4;
            if (data[x][i * 8 + 6]) b |=   2;
            if (data[x][i * 8 + 7]) b |=   1;

            bytes += b;
        }
    }

    return bytes;
}

void HashPrint::toBytes(uchar* target[32])
{
    memcpy(target, toBytes().begin(), sizeof(uchar) * 32);
}

QPixmap HashPrint::draw(int borderSize, int cellSize, const QColor& color)
{
    QImage hashPic(16 * cellSize + borderSize * 2, 16 * cellSize + borderSize * 2, QImage::Format::Format_RGBA8888);

    hashPic.fill(QColor(Qt::GlobalColor::transparent));
    for (int i = 0; i < hashPic.width(); i++)
    {
        for (int j = 0; j < borderSize; j++)
        {
            hashPic.setPixelColor(i, j,                        color);
            hashPic.setPixelColor(i, hashPic.height() - 1 - j, color);
        }
    }
    for (int i = 0; i < hashPic.height(); i++)
    {
        for (int j = 0; j < borderSize; j++)
        {
            hashPic.setPixelColor(j,                       i, color);
            hashPic.setPixelColor(hashPic.width() - 1 - j, i, color);
        }
    }
    for (uchar y = 0; y < 16; y++)
    {
        for (uchar x = 0; x < 16; x++)
        {
            if (data[x][y])
            {
                for (int i = 0; i < cellSize; i++)
                {
                    for (int j = 0; j < cellSize; j++)
                    {
                        hashPic.setPixelColor(borderSize + x * cellSize + i, borderSize + y * cellSize + j, color);
                    }
                }
            }
        }
    }

    return QPixmap::fromImage(hashPic);
}

bool HashPrint::get(uchar x, uchar y) const
{
    return data[x][y];
}

void HashPrint::set(uchar x, uchar y, bool value)
{
    data[x][y] = value;
}

bool HashPrint::operator ==(const HashPrint& rhs)
{
    for (uchar x = 0; x < 16; x++)
    {
        for (uchar y = 0; y < 16; y++)
        {
            if (data[x][y] != rhs.data[x][y]) return false;
        }
    }

    return true;
}
