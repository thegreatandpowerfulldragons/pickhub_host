#include "itemview.h"
#include <QScrollBar>
#include <QHBoxLayout>
#include <QPainter>
#include <QtMath>
#include <QWheelEvent>
#include "application.h"
#include "dss.h"
#include <QMenu>

#define borderWidth 4

ItemView::ItemView(QWidget* parent)
    : QWidget(parent)
    , itemSize(350)
    , itemSpace(10)
    , displayMode(DisplayMode::All)
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setMargin(0);
    setLayout(layout);

    spacer = new QWidget(this);
    spacer->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum));
    layout->addWidget(spacer);

    scrollbar = new QScrollBar(Qt::Orientation::Vertical, this);
    scrollbar->setSingleStep(getBoxSize() + itemSpace);
    layout->addWidget(scrollbar);

    recalculateRowDimensions();

    setFocusPolicy(Qt::FocusPolicy::WheelFocus);

    connect(scrollbar, &QScrollBar::valueChanged, [=](int){
        update();
    });
    connect(Application::getDss(), SIGNAL(updated()), this, SLOT(update()));
}

ItemView::~ItemView()
{
    thumbnails.clear();
}

void ItemView::setupThumbnails(const QMap<int, Item>& thumbnails)
{
    displayMode = DisplayMode::All;
    displayItems.clear();

    this->thumbnails = thumbnails;
    updateModel();
    update();
}

void ItemView::addThumbnail(int id, const Item& thumbnail, bool display)
{
    if (thumbnails.contains(id)) return;

    thumbnails[id] = thumbnail;

    if (model.last().length() < cachedColumnCount)
    {
        model.last() += id;
    }
    else
    {
        model += QVector<int>{ id };
    }

    if ((displayMode == DisplayMode::Specific && display) || displayMode == DisplayMode::All)
    {
        if (displayMode == DisplayMode::Specific && display)
        {
            displayItems += id;
        }

        update();
    }
}

void ItemView::updateThumbnail(int id, const QPixmap& thumbnail)
{
    if (!thumbnails.contains(id)) return;

    thumbnails[id].pixmap = thumbnail;

    if ((displayMode == DisplayMode::Specific && displayItems.contains(id)) || displayMode == DisplayMode::All)
    {
        update();
    }
}

void ItemView::removeThumbnail(int id)
{
    if (!thumbnails.contains(id)) return;

    thumbnails.remove(id);

    if ((displayMode == DisplayMode::Specific && displayItems.contains(id)) || displayMode == DisplayMode::All)
    {
        if (displayMode == DisplayMode::Specific && displayItems.contains(id))
        {
            displayItems.removeAll(id);
        }

        // Much easier than recalculate whole model
        bool found = false;
        for (int row = 0; row < model.length(); row++)
        {
            if (!found)
            {
                if (model[row].contains(id))
                {
                    model[row].removeOne(id);
                    found = true;
                }
            }

            if (found && row < model.length() - 1)
            {
                model[row] += model[row + 1].first();
                model[row + 1].removeFirst();
            }
        }
        updateModel();

        update();
    }
}

int ItemView::getItemSize() const
{
    return itemSize;
}

void ItemView::setItemSize(int value)
{
    if (itemSize == value) return;

    itemSize = value;

    scrollbar->setSingleStep(getBoxSize() + itemSpace);

    int oldCount = cachedColumnCount;
    recalculateRowDimensions();
    if (oldCount != cachedColumnCount)
    {
        updateModel();
    }
    update();
}

int ItemView::getItemSpace() const
{
    return itemSpace;
}

void ItemView::setItemSpace(int value)
{
    if (itemSpace == value) return;

    itemSpace = value;

    scrollbar->setSingleStep(getBoxSize() + itemSpace);

    int oldCount = cachedColumnCount;
    recalculateRowDimensions();
    if (oldCount != cachedColumnCount)
    {
        updateModel();
    }
    update();
}

void ItemView::DisplaySpecific(const QVector<int>& indexes)
{
    displayMode = DisplayMode::Specific;
    displayItems = indexes;

    updateModel();

    if (!indexes.contains(selectedId))
    {
        select({-1, -1});
    }
    else
    {
        update();
    }
}

void ItemView::displayAll()
{
    displayMode = DisplayMode::All;
    displayItems.clear();

    updateModel();

    update();
}

ItemView::DisplayMode ItemView::getDisplayMode() const
{
    return displayMode;
}

void ItemView::drawItem(QPainter& painter, const ItemView::Item& item, const QRect& rect, const QColor& mainColor, const QColor& textColor)
{
    QRect thumbnailRect = item.pixmap.rect();
    thumbnailRect.moveCenter(rect.center());

    painter.setPen(QPen(mainColor, borderWidth * 2, Qt::PenStyle::SolidLine, Qt::PenCapStyle::FlatCap, Qt::PenJoinStyle::RoundJoin));
    painter.drawRect(thumbnailRect);
    painter.drawPixmap(thumbnailRect.topLeft(), item.pixmap);

    QString title;
    switch (item.type)
    {
        case Collection::ItemType::Animation:
            title = "ANIM";
            break;

        case Collection::ItemType::Video:
            title = "VIDEO";
            break;

        case Collection::ItemType::Audio:
            title = "AUDIO";
            break;

        default:
            title = "";
            break;
    }
    if (!title.isEmpty())
    {
        QSize titleSize = QFontMetrics(painter.font()).boundingRect(title).size() + QSize(10, 0);
        QRect titleRect = QRect(QPoint(thumbnailRect.right() - titleSize.width() + 2, thumbnailRect.top()), titleSize);

        QPainterPath titlePath(titleRect.topLeft());
        titlePath.lineTo(titleRect.bottomLeft() - QPoint(0, 10));
        titlePath.arcTo(QRect(titlePath.currentPosition().toPoint(), QSize(10, 10)), 180, 90);
        titlePath.lineTo(titleRect.bottomRight());
        titlePath.lineTo(titleRect.topRight());
        painter.fillPath(titlePath, mainColor);

        painter.setPen(textColor);
        painter.drawText(titleRect.bottomLeft() + QPointF(5, -3), title);
    }
}

void ItemView::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setClipRect(getDisplayRect());
    painter.setRenderHint(QPainter::RenderHint::Antialiasing);

    int rowHeight = cachedCellSize.height() + itemSpace;

    int firstVisibleRow = qFloor(scrollbar->value() / rowHeight);
    int lastVisibleRow = qMin(qFloor((scrollbar->value() + getDisplayRect().height()) / (cachedCellSize.height() + itemSpace)) + 1, model.length() - 1);

    QColor normalBorderColor = Qt::GlobalColor::white;
    QColor activeBorderColor = Application::getDss()->getParam("appColor");

    QColor normalTextColor = Application::getDss()->getParam("appColor");
    QColor activeTextColor = Qt::GlobalColor::white;

    for (int i = firstVisibleRow; i <= lastVisibleRow; i++)
    {
        for (int j = 0; j < model[i].length(); j++)
        {
            QRect cellRect = QRect(QPoint((cachedCellSize.width() + itemSpace) * j, (firstVisibleRow * rowHeight) - scrollbar->value() + rowHeight * (i - firstVisibleRow)), cachedCellSize.toSize());

            bool selected = model[i][j] == selectedId;
            drawItem(painter, thumbnails[model[i][j]], cellRect, selected ? activeBorderColor : normalBorderColor, selected ? activeTextColor : normalTextColor);
        }
    }
}

void ItemView::resizeEvent(QResizeEvent*)
{
    int oldCount = cachedColumnCount;
    recalculateRowDimensions();
    if (oldCount != cachedColumnCount)
    {
        updateModel();
    }
    update();
}

void ItemView::wheelEvent(QWheelEvent* event)
{
    scrollbar->setValue(scrollbar->value() - event->angleDelta().y());
}

void ItemView::mousePressEvent(QMouseEvent* event)
{
    select(getItemUnderPoint(event->pos()));
}

void ItemView::mouseDoubleClickEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
    {
        emit itemOpen(selectedId);
    }
}

void ItemView::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
    case Qt::Key::Key_Enter:
    case Qt::Key::Key_Return:
    case Qt::Key::Key_Space:
        emit itemOpen(selectedId);
        break;

    case Qt::Key::Key_Delete:
    case Qt::Key::Key_Backspace:
        emit itemDelete(selectedId);
        break;

    case Qt::Key::Key_Up:
        if (selection.row > 0 && selection.col < model[selection.row - 1].length())
        {
            select(selection.up());
        }
        break;

    case Qt::Key::Key_Down:
        if (selection.row < model.length() - 1 && selection.col < model[selection.row + 1].length())
        {
            select(selection.down());
        }
        break;

    case Qt::Key::Key_Left:
        if (selection.col > 0)
        {
            select(selection.left());
        }
        break;

    case Qt::Key::Key_Right:
        if (selection.col < model[selection.row].length() - 1)
        {
            select(selection.right());
        }
        break;

    default:
        event->ignore();
        break;
    }
}

void ItemView::contextMenuEvent(QContextMenuEvent* event)
{
    if (selectedId >= 0)
    {
        showContext(event->globalPos());
    }
}

QRect ItemView::getDisplayRect() const
{
    return QRect(QPoint(), spacer->size());
}

int ItemView::getBoxSize() const
{
    return itemSize + borderWidth * 2;
}

ItemView::Selection ItemView::getItemUnderPoint(const QPoint& point)
{
    QPoint scrolledPoint = point + QPoint(0, scrollbar->value());

    int row = scrolledPoint.y() / (cachedCellSize.height() + itemSpace);
    int col = scrolledPoint.x() / (cachedCellSize.width() + itemSpace);

    if (row >= model.length() || col >= model[row].length())
    {
        return {-1, -1};
    }

    QRect cellRect = QRect(QPoint(col * (cachedCellSize.width() + itemSpace), row * (cachedCellSize.height() + itemSpace)), cachedCellSize.toSize());
    QRect thumbnailRect = thumbnails[model[row][col]].pixmap.rect() + QMargins(borderWidth, borderWidth, borderWidth, borderWidth);

    thumbnailRect.moveCenter(cellRect.center());

    if (thumbnailRect.contains(scrolledPoint))
    {
        return {row, col};
    }
    else
    {
        return {-1, -1};
    }
}

void ItemView::select(const ItemView::Selection& selection)
{
    this->selection = selection;
    selectedId = (selection.row >= 0 && selection.row < model.length() && selection.col >= 0 && model[selection.row].length()) ? model[selection.row][selection.col] : -1;
    emit itemSelected(selectedId);
    update();
}

void ItemView::showContext(const QPoint& position)
{
#define action(name, displayName, signal)\
QAction* name##Action = new QAction(tr(displayName));\
menu->addAction(name##Action);\
connect(name##Action, &QAction::triggered, [=](){\
    emit signal(selectedId);\
});

    QMenu* menu = new QMenu(this);

    action(open, "Open", itemOpen);
    action(locate, "Locate", itemLocate);
    action(delete, "Delete", itemDelete);

    menu->addSeparator();

    action(openSource, "Open source", openSource);
    action(searchWithSameHashprint, "Search with same hashprint", searchWithSameHashprint);

    menu->addSeparator();

    action(addToAnotherCollection, "Add to another collection", addToAnotherCollection);
    action(moveToAnotherCollection, "Move to another collection", moveToAnotherCollection);

    menu->exec(position);

#undef action
}

void ItemView::updateModel()
{
    model.clear();

    QVector<int> row;
    if (displayMode == DisplayMode::All)
    {
        for (int key : thumbnails.keys())
        {
            row += key;
            if (row.length() >= cachedColumnCount)
            {
                model += row;
                row.clear();
            }
        }
    }
    else if (displayMode == DisplayMode::Specific)
    {
        for (int key : thumbnails.keys())
        {
            if (displayItems.contains(key))
            {
                row += key;
                if (row.length() >= cachedColumnCount)
                {
                    model += row;
                    row.clear();
                }
            }
        }
    }
    if (!row.isEmpty()) model += row;

    QRect displayRect = getDisplayRect();
    scrollbar->setPageStep(displayRect.height());
    scrollbar->setMaximum(qMax(model.length() * getBoxSize() + (model.length() - 1) * itemSpace - scrollbar->pageStep(), 0));
}

void ItemView::addItemToModel()
{

}

void ItemView::recalculateRowDimensions()
{
    int limit = getDisplayRect().width();
    int measure = getBoxSize();
    cachedColumnCount = 0;

    while (measure < limit)
    {
        measure += itemSpace + getBoxSize();
        cachedColumnCount++;
    }

    cachedCellSize = QSizeF((float)(limit - itemSpace * (cachedColumnCount - 1)) / cachedColumnCount, getBoxSize());
}
